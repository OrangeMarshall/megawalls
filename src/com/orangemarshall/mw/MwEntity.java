package com.orangemarshall.mw;

public abstract class MwEntity{

    private long lastDeath = -1;

    public long getLastDeath(){
        return lastDeath;
    }

    public void spawn(){
        
    }

    public void die(){
        lastDeath = System.currentTimeMillis();
    }

}
