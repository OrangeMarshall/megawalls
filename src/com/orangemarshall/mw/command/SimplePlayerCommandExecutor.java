package com.orangemarshall.mw.command;

import com.google.common.collect.Lists;
import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.util.PluginHolder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;

import java.util.List;


public abstract class SimplePlayerCommandExecutor extends PluginHolder implements CommandExecutor{

    private final List<String> aliases;
    private final String name;

    /*
        Aliases must be all lowercase
     */
    public SimplePlayerCommandExecutor(MegaWalls plugin, String name, String... aliases){
        super(plugin);
        this.name = name;
        this.aliases = Lists.newArrayList(aliases);
    }

    public void enable(){
        PluginCommand cmd = plugin.getCommand(name);
        cmd.setExecutor(this);
        cmd.setAliases(this.aliases);
    }

    public void disable(){
        PluginCommand cmd = plugin.getCommand(name);
        cmd.setExecutor(null);
        cmd.setAliases(Lists.newArrayList());
    }

    @Override
    public final boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        if(name.equalsIgnoreCase(label.toLowerCase()) || aliases.contains(label.toLowerCase())){
            if(commandSender instanceof Player){
                return execute((Player) commandSender, args);
            }
        }

        return false;
    }

    protected abstract boolean execute(Player player, String[] args);

}
