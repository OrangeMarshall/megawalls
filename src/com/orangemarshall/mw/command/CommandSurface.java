package com.orangemarshall.mw.command;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.util.CachedPlayerCooldown;
import com.orangemarshall.util.Cooldown;
import com.orangemarshall.util.Notifier;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.concurrent.TimeUnit;


public class CommandSurface extends SimplePlayerCommandExecutor{

    private CachedPlayerCooldown cooldownCache = new CachedPlayerCooldown(25, TimeUnit.SECONDS);

    public CommandSurface(MegaWalls plugin){
        super(plugin, "surface");
    }

    @Override
    protected boolean execute(Player player, String[] args){
        Cooldown cooldown = cooldownCache.getCooldownFor(player);

        if(cooldown.attemptReset()){
            surface(player);
        }else{
            long milisToWait = cooldown.remainingTime();

            String seconds = Util.timeToSingleUnit(milisToWait, TimeUnit.MILLISECONDS, TimeUnit.SECONDS);
            String message = ChatColor.RED + "You must wait " + seconds + " before using /surface again!";

            new Notifier().toPlayer(player).send(message);
        }

        return true;
    }

    private void surface(Player player){
        Location playerLocation = player.getLocation();
        Location surfaceLocation = playerLocation.getWorld().getHighestBlockAt(player.getLocation()).getLocation();

        surfaceLocation.setYaw(playerLocation.getYaw());
        surfaceLocation.setPitch(playerLocation.getPitch());

        player.teleport(surfaceLocation);
    }

}
