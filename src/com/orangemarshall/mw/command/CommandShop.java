package com.orangemarshall.mw.command;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.gui.MainMenu;
import com.orangemarshall.util.gui.GuiController;
import com.orangemarshall.util.gui.screen.GuiScreen;
import org.bukkit.entity.Player;


public class CommandShop extends SimplePlayerCommandExecutor{

    public CommandShop(MegaWalls plugin){
        super(plugin, "shop");
    }

    @Override
    protected boolean execute(Player player, String[] args){
        GuiController holder = new GuiController(plugin);
        GuiScreen screen = new MainMenu(holder);

        holder.openGui(screen, player);

        return true;
    }

}
