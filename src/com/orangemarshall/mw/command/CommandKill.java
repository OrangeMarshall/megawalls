package com.orangemarshall.mw.command;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.util.CachedPlayerCooldown;
import com.orangemarshall.util.Cooldown;
import com.orangemarshall.util.Notifier;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.concurrent.TimeUnit;


public class CommandKill extends SimplePlayerCommandExecutor{

    private CachedPlayerCooldown cooldownCache = new CachedPlayerCooldown(60, TimeUnit.SECONDS);

    public CommandKill(MegaWalls plugin){
        super(plugin, "kill");
    }

    @Override
    protected boolean execute(Player player, String[] args){
        Cooldown cooldown = cooldownCache.getCooldownFor(player);

        if(cooldown.attemptReset()){
            kill(player);
        }else{
            long milisToWait = cooldown.remainingTime();

            String seconds = Util.timeToSingleUnit(milisToWait, TimeUnit.MILLISECONDS, TimeUnit.SECONDS);
            String message = ChatColor.RED + "You must wait " + seconds + " before using /kill again!";

            new Notifier().toPlayer(player).send(message);
        }

        return true;
    }

    private void kill(Player player){
        player.setHealth(0.0);
    }

}
