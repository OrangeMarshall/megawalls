package com.orangemarshall.mw;

import com.orangemarshall.mw.clazz.*;
import com.orangemarshall.mw.clazz.energy.EnergyHolder;
import com.orangemarshall.mw.event.MwPlayerChangeClassEvent;
import com.orangemarshall.mw.event.MwPlayerEnergyEvent;
import org.bukkit.entity.Player;


public class MwPlayer extends MwEntity implements EnergyHolder{

    private final MwPlayerRegistry registry;

    public final Player player;

    private MwClassEnum selectedClass = MwClassEnum.ZOMBIE;
    private MwClass mwClassInstance;

    private Upgrades classUpgrades = new Upgrades(9, 9, 9, 9, 9);

    private float energy = 0;

    public MwPlayer(MwPlayerRegistry registry, Player player){
        this.player = player;
        this.registry = registry;
        this.mwClassInstance = MwFactory.createClassInstance(selectedClass, registry.getPlugin(), this);
    }

    public Upgrades getClassUpgrades(){
        return classUpgrades;
    }

    public MwPlayerRegistry getRegistry(){
        return registry;
    }

    public MwClass getMwClassInstance(){
        return mwClassInstance;
    }

    public void changeMwClass(MwClassEnum classEnum){
        this.selectedClass = classEnum;
        registry.getPlugin().callEvent(new MwPlayerChangeClassEvent(this));
    }

    @Override
    public void die(){
        mwClassInstance.die();
    }

    @Override
    public void spawn(){
        if(selectedClass != mwClassInstance.getClassEnum()){
            this.mwClassInstance = MwFactory.createClassInstance(selectedClass, registry.getPlugin(), this);
        }

        this.mwClassInstance.spawn();

        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
    }

    public float getEnergy(){
        return energy;
    }

    public void addEnergy(float energy){
        float prevEnergy = this.energy;

        this.energy += energy;

        if(this.energy > 100.0F){
            this.energy = 100.0F;
        }

        if(prevEnergy != this.energy){
            registry.getPlugin().callEvent(new MwPlayerEnergyEvent(this));
        }
    }

}
