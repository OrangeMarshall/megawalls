package com.orangemarshall.mw.clazz.energy;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.event.MwPlayerDealMeleeDamageEvent;
import org.bukkit.event.EventHandler;


public class MeleeEnergyListener extends EnergyListener{

    private float energy;

    public MeleeEnergyListener(MegaWalls plugin, MwPlayer mwPlayer, float energy){
        super(plugin, mwPlayer);
        this.energy = energy;
    }

    @EventHandler
    public void onMelee(MwPlayerDealMeleeDamageEvent event){
        if(event.mwPlayer == mwPlayer){
            addEnergy(energy);
        }
    }


}
