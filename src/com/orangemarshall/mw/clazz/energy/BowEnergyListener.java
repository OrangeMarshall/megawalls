package com.orangemarshall.mw.clazz.energy;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.event.MwPlayerDealBowDamageEvent;
import org.bukkit.event.EventHandler;


public class BowEnergyListener extends EnergyListener{

    private float energy;

    public BowEnergyListener(MegaWalls plugin, MwPlayer mwPlayer, float energy){
        super(plugin, mwPlayer);
        this.energy = energy;
    }

    @EventHandler
    public void onBow(MwPlayerDealBowDamageEvent event){
        if(event.mwPlayer == mwPlayer){
            addEnergy(energy);
        }
    }


}
