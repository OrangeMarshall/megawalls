package com.orangemarshall.mw.clazz.energy;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.util.SimpleListener;


public abstract class EnergyListener extends SimpleListener{

    protected MwPlayer mwPlayer;

    public EnergyListener(MegaWalls plugin, MwPlayer mwPlayer){
        super(plugin);
        this.mwPlayer = mwPlayer;
    }

    protected void addEnergy(float energy){
        mwPlayer.addEnergy(energy);
    }

}
