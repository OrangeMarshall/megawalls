package com.orangemarshall.mw.clazz.energy;

public interface EnergyHolder{

    float getEnergy();
    void addEnergy(float energy);

}
