package com.orangemarshall.mw.clazz.energy;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.event.MwPlayerEnergyEvent;
import com.orangemarshall.mw.util.SimpleListener;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;


public class EnergyVisualizerListener extends SimpleListener{

    public EnergyVisualizerListener(MegaWalls plugin){
        super(plugin);
        plugin.getServer().getOnlinePlayers().forEach(player -> setEnergy(player, 0f));
    }

    @EventHandler
    public void onEnergy(PlayerExpChangeEvent event){
        event.setAmount(0);
    }

    @EventHandler
    public void onLogin(PlayerJoinEvent event){
        setEnergy(event.getPlayer(), 0);
    }

    @EventHandler
    public void onEnergy(MwPlayerEnergyEvent event){
        float energy = event.mwPlayer.getEnergy();
        Player player = event.mwPlayer.player;

        setEnergy(player, energy);
    }

    private void setEnergy(Player player, float energy){
        float percentage = energy / 100.0f;
        percentage = Math.max(0.01f, Math.min(99.9f, percentage));

        player.setLevel((int) energy);
        player.setExp(percentage);
    }

}
