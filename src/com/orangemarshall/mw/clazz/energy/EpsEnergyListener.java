package com.orangemarshall.mw.clazz.energy;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;


public class EpsEnergyListener extends EnergyListener{

    private EpsRunnable runnable;
    private int taskId = -1;

    public EpsEnergyListener(MegaWalls plugin, MwPlayer mwPlayer, float energy){
        super(plugin, mwPlayer);

        this.runnable = new EpsRunnable(mwPlayer, energy);
    }

    @Override
    public void register(){
        if(taskId < 0){
            taskId = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, runnable, 20L, 20L);
        }
    }

    @Override
    public void unregister(){
        if(taskId >= 0){
            plugin.getServer().getScheduler().cancelTask(taskId);
            taskId = -1;
        }
    }

    private static class EpsRunnable implements Runnable{

        private MwPlayer mwPlayer;
        private float energy;

        private EpsRunnable(MwPlayer mwPlayer, float energy){
            this.mwPlayer = mwPlayer;
            this.energy = energy;
        }

        @Override
        public void run(){
            if(canGiveEnergy()){
                mwPlayer.addEnergy(energy);
            }
        }

        private boolean canGiveEnergy(){
            return mwPlayer != null && mwPlayer.player != null && mwPlayer.player.isOnline();
        }

    }


}
