package com.orangemarshall.mw.clazz.skill;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;


public interface SkillSetProvider{

    SkillSet create(MegaWalls plugin, MwPlayer mwPlayer);
    SkillSet createDummy();

}
