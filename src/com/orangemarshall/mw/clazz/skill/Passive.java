package com.orangemarshall.mw.clazz.skill;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.util.SimpleListener;


public abstract class Passive extends SimpleListener{

    protected final int level;
    protected final MwPlayer mwPlayer;

    public Passive(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(plugin);
        this.mwPlayer = mwPlayer;
        this.level = level;
    }

    public abstract String getName();
    public abstract String getDescription(int level);

    public String getDescription(){
        return getDescription(level);
    }

}
