package com.orangemarshall.mw.clazz.skill;

import com.orangemarshall.mw.MwPlayer;


public abstract class PlayerAbility {

    protected final int level;
    protected final MwPlayer mwPlayer;

    public PlayerAbility(MwPlayer mwPlayer, int level){
        this.mwPlayer = mwPlayer;
        this.level = level;
    }

    public abstract void register();
    public abstract void unregister();

    public abstract String getDescription(int level);

    public String getDescription(){
        return getDescription(level);
    }

    public abstract String getName();

    public abstract boolean attemptUse();

}
