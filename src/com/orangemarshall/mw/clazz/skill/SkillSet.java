package com.orangemarshall.mw.clazz.skill;

import com.orangemarshall.mw.clazz.Kit;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.mw.clazz.skill.PlayerAbility;


public class SkillSet{

    private PlayerAbility ability;
    private Passive passive1;
    private Passive passive2;
    private Kit kit;
    private Passive gathering;

    public SkillSet(PlayerAbility ability, Passive passive1, Passive passive2, Kit kit, Passive gathering){
        this.ability = ability;
        this.passive1 = passive1;
        this.passive2 = passive2;
        this.kit = kit;
        this.gathering = gathering;
    }

    public PlayerAbility getAbility(){
        return ability;
    }

    public Passive getPassive1(){
        return passive1;
    }

    public Passive getPassive2(){
        return passive2;
    }

    public Kit getKit(){
        return kit;
    }

    public Passive getGathering(){
        return gathering;
    }

    public void registerAll(){
        ability.register();
        passive1.register();
        passive2.register();
        gathering.register();
    }

    public void unregisterAll(){
        ability.unregister();
        passive1.unregister();
        passive2.unregister();
        gathering.unregister();
    }

}
