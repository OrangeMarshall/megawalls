package com.orangemarshall.mw.clazz;

public class SimpleDescriptable implements Descriptable{

    private String name, description;

    public SimpleDescriptable(String name, String description){
        this.name = name;
        this.description = description;
    }

    @Override
    public String getName(){
        return null;
    }

    @Override
    public String getDescription(){
        return null;
    }
}
