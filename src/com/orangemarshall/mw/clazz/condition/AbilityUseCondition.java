package com.orangemarshall.mw.clazz.condition;

import com.orangemarshall.mw.MwPlayer;
import org.bukkit.Material;


public abstract class AbilityUseCondition{

    private MwPlayer mwPlayer;

    public AbilityUseCondition(MwPlayer mwPlayer){
        this.mwPlayer = mwPlayer;
    }

    public abstract boolean attemptUse(ClickType clickType, Material heldItem);

    protected boolean hasEnergy(float amount){
        return mwPlayer.getEnergy() >= amount;
    }

    public enum ClickType{
        RIGHT, LEFT
    }

}
