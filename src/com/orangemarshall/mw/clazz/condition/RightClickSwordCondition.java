package com.orangemarshall.mw.clazz.condition;

import com.orangemarshall.mw.MwPlayer;
import org.bukkit.Material;


public class RightClickSwordCondition extends AbilityUseCondition{

    public RightClickSwordCondition(MwPlayer mwPlayer){
        super(mwPlayer);
    }

    @Override
    public boolean attemptUse(ClickType clickType, Material heldItem){
        if(hasEnergy(100.0f)){
            if(clickType == ClickType.RIGHT){
                if(isSword(heldItem)){
                    return true;
                }
            }
        }

        return false;
    }

    private boolean isSword(Material material){
        return material == Material.DIAMOND_SWORD || material == Material.GOLD_SWORD
                || material == Material.IRON_SWORD || material == Material.WOOD_SWORD
                || material == Material.STONE_SWORD;
    }
}
