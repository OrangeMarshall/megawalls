package com.orangemarshall.mw.clazz.condition;

import com.orangemarshall.mw.MwPlayer;
import org.bukkit.Material;


public class LeftClickBowCondition extends AbilityUseCondition{

    public LeftClickBowCondition(MwPlayer mwPlayer){
        super(mwPlayer);
    }

    @Override
    public boolean attemptUse(ClickType clickType, Material heldItem){
        if(hasEnergy(100.0f)){
            if(clickType == ClickType.LEFT){
                if(heldItem == Material.BOW){
                    return true;
                }
            }
        }
        return false;
    }

}
