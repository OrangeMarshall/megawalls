package com.orangemarshall.mw.clazz;

import org.bukkit.inventory.ItemStack;

import java.util.List;


public abstract class Kit{

    protected int level;

    public Kit(int level){
        this.level = level;
    }

    public int getLevel(){
        return level;
    }

    public abstract List<ItemStack> getCopy();

}
