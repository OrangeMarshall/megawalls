package com.orangemarshall.mw.clazz;

public class Upgrades{

    private int[] upgrades = new int[5];

    public Upgrades(int ability, int passive1, int passive2, int kit, int gathering){
        upgrades[0] = ability;
        upgrades[1] = passive1;
        upgrades[2] = passive2;
        upgrades[3] = kit;
        upgrades[4] = gathering;
    }

    public int getAbilityLevel(){
        return upgrades[0];
    }

    public int getPassive1Level(){
        return upgrades[1];
    }

    public int getPassive2Level(){
        return upgrades[2];
    }

    public int getKitLevel(){
        return upgrades[3];
    }

    public int getGatheringLevel(){
        return upgrades[4];
    }

}
