package com.orangemarshall.mw.clazz.clazzes.blaze;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.mw.entity.FriendlyBlaze;
import com.orangemarshall.mw.entity.MwEntityRegistry;
import com.orangemarshall.mw.event.MwPlayerKillEvent;
import com.orangemarshall.util.EntityUtil;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;

import java.util.concurrent.TimeUnit;


public class CallOfTheBlazesPassive extends Passive{

    public CallOfTheBlazesPassive(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);
    }

    @Override
    public String getName(){
        return "Call of the Blazes";
    }

    @EventHandler
    public void onKill(MwPlayerKillEvent event){
        if(event.mwPlayer == mwPlayer){
            if(doesActivate()){
                activate();
            }
        }
    }

    private void activate(){
        Location location = mwPlayer.player.getLocation();

        for(int i = 0; i < getBlazeCount(level); i++){
            FriendlyBlaze blaze = new FriendlyBlaze(location.getWorld());

            blaze.addFriendly(mwPlayer.player);

            MwEntityRegistry.spawn(blaze, location);
            EntityUtil.respawnAfter(blaze.getBukkitEntity(), 15, TimeUnit.SECONDS, plugin);
        }
    }

    private boolean doesActivate(){
        return Util.shouldActivate(getActivationChance(level));
    }

    @Override
    public String getDescription(int level){
        int blazeCount = getBlazeCount(level);
        String blazes = blazeCount == 1 ? "1 Blaze" : blazeCount + " Blazes";
        return ChatColor.GREEN.toString() + getActivationChance(level) + "% " + ChatColor.GRAY + " chance to spawn " + ChatColor.GREEN + blazes + ChatColor.GRAY + " when you kill an enemy.";
    }

    private int getBlazeCount(int level){
        return level == 9 ? 2 : 1;
    }

    private float getActivationChance(int level){
        return 20.0F + 10.0F*(level-1);
    }

}
