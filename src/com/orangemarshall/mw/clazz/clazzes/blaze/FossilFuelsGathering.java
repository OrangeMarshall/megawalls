package com.orangemarshall.mw.clazz.clazzes.blaze;

import com.google.common.collect.Sets;
import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Set;


public class FossilFuelsGathering extends Passive{

    private Set<Material> typesToTrigger = Sets.newHashSet();

    public FossilFuelsGathering(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);

        typesToTrigger.add(Material.IRON_ORE);
    }

    @Override
    public String getName(){
        return "Well Trained";
    }

    @EventHandler
    public void onMine(BlockBreakEvent event){
        if(event.getPlayer() == mwPlayer.player){
            Material material = event.getBlock().getType();

            if(typesToTrigger.contains(material) && doesActivate()){
                activate();
            }
        }
    }

    private void activate(){
        mwPlayer.player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Util.secondsToTicks(5), 1));
    }

    private boolean doesActivate(){
        return Util.shouldActivate(getActivationChance(level));
    }

    @Override
    public String getDescription(int level){
        return ChatColor.GREEN.toString() + getActivationChance(level) + "%" + ChatColor.GRAY + " chance to get Regeneration II for 5 seconds when you mine ores.";
    }

    private float getActivationChance(int level){
        return 50.0F + 6.25F*(level-1);
    }
}
