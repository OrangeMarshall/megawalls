package com.orangemarshall.mw.clazz.clazzes.blaze;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class FireArrowsPassive extends Passive{

    public FireArrowsPassive(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);
    }

    @Override
    public String getName(){
        return "Fire Arrows";
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event){
        if(event.getDamager() instanceof Arrow){
            Arrow arrow = (Arrow) event.getDamager();

            if(arrow.getShooter() == mwPlayer.player && event.getEntity() instanceof Player){
                if(doesActivate()){
                    activate((Player) event.getEntity());
                }
            }
        }
    }


    private void activate(Player player){
        player.setFireTicks(Util.secondsToTicks(3));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Util.secondsToTicks(5), 0));
    }

    private boolean doesActivate(){
        return Util.shouldActivate(getActivationChance(level));
    }

    @Override
    public String getDescription(int level){
        return ChatColor.GRAY + "Getting hit by an arrow has a " + ChatColor.GREEN + getActivationChance(level) + "%" + ChatColor.GRAY + " chance to give you Strength I and Speed II for 3 seconds.";
    }

    private float getActivationChance(int level){
        return 10.0F + 2.5F*(level-1);
    }

}
