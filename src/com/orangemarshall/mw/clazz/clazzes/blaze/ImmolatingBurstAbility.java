package com.orangemarshall.mw.clazz.clazzes.blaze;

import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.PlayerAbility;
import com.orangemarshall.util.CountingRepeatingTask;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.SmallFireball;
import org.bukkit.util.Vector;


public class ImmolatingBurstAbility extends PlayerAbility{

    public ImmolatingBurstAbility(MwPlayer owner, int level){
        super(owner, level);
    }

    @Override
    public void register(){}

    @Override
    public void unregister(){}

    @Override
    public String getDescription(int level){
        return ChatColor.GRAY + "Shoot 3 fireballs, each hitting a single target for " + ChatColor.GREEN + damage(level) + ChatColor.GRAY + " damage.";
    }

    @Override
    public String getName(){
        return "Immolating Burst";
    }

    @Override
    public boolean attemptUse(){
        mwPlayer.player.getWorld().playSound(mwPlayer.player.getLocation(), Sound.BLAZE_BREATH, 1.0f, 1.0f);

        new CountingRepeatingTask(mwPlayer.getRegistry().getPlugin())
                .runTaskSync(new BurstRunnable(mwPlayer), 20, 15, 3);
        return true;
    }

    //TODO implement dmg
    private float damage(){
        return damage(level);
    }

    private float damage(int level){
        return 6.0f + (level-1)*0.5f;
    }

    private static class BurstRunnable implements Runnable{

        private MwPlayer mwPlayer;
        private long lastDeath;

        private BurstRunnable(MwPlayer mwPlayer){
            this.mwPlayer = mwPlayer;
            this.lastDeath = mwPlayer.getLastDeath();
        }

        @Override
        public void run(){
            if(lastDeath == mwPlayer.getLastDeath()){
                Vector direction = mwPlayer.player.getEyeLocation().getDirection();
                mwPlayer.player.launchProjectile(SmallFireball.class, direction);
                mwPlayer.player.getWorld().playSound(mwPlayer.player.getLocation(), Sound.GHAST_FIREBALL, 1.0f, 1.0f);
            }
        }
    }

}
