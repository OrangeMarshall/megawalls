package com.orangemarshall.mw.clazz.clazzes.zombie;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.mw.event.MwPlayerReceiveBowDamageEvent;
import com.orangemarshall.mw.event.MwPlayerReceiveMeleeDamageEvent;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class ToughnessPassive extends Passive{

    private Random random = ThreadLocalRandom.current();

    public ToughnessPassive(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);
    }

    @Override
    public String getName(){
        return "Toughness";
    }

    @EventHandler
    public void onDamage(MwPlayerReceiveBowDamageEvent event){
        if(event.mwPlayer == mwPlayer){
            if(doesActivate()){
                activate(event.event);
            }
        }
    }

    @EventHandler
    public void onDamage(MwPlayerReceiveMeleeDamageEvent event){
        if(event.mwPlayer == mwPlayer){
            if(doesActivate()){
                activate(event.event);
                mwPlayer.player.sendMessage(ChatColor.YELLOW + "Your " + getName() + " passive has activated!");
            }
        }
    }

    private void activate(EntityDamageByEntityEvent event){
        event.setDamage(EntityDamageEvent.DamageModifier.RESISTANCE, -0.2);
    }

    private boolean doesActivate(){
        return Util.shouldActivate(getActivationChance());
    }

    @Override
    public String getDescription(int level){
        return ChatColor.GRAY + "When you get hit you have a " + ChatColor.GREEN + getActivationChance(level) + "%" + ChatColor.GRAY + " chance to gain Resistance I.";
    }

    private float getActivationChance(){
        return getActivationChance(level);
    }

    private float getActivationChance(int level){
        return 10.0F + 3.4375f*(level-1);
    }

}
