package com.orangemarshall.mw.clazz.clazzes.zombie;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.MwClass;
import com.orangemarshall.mw.clazz.MwClassEnum;
import com.orangemarshall.mw.clazz.MwClassInformation;
import com.orangemarshall.mw.clazz.condition.LeftClickBowCondition;
import com.orangemarshall.mw.clazz.condition.RightClickSwordCondition;
import com.orangemarshall.mw.clazz.energy.MeleeEnergyListener;


public final class Zombie extends MwClass{

    public Zombie(MwClassEnum classEnum, MegaWalls plugin, MwPlayer mwPlayer){
        super(classEnum, plugin, mwPlayer);
        this.mwPlayer = mwPlayer;

        this.abilityUseConditions.add(new RightClickSwordCondition(mwPlayer));
        this.abilityUseConditions.add(new LeftClickBowCondition(mwPlayer));

        this.energyListeners.add(new MeleeEnergyListener(plugin, mwPlayer, 20.0f));
    }

    public static class Description extends MwClassInformation{

        @Override
        public String getName(){
            return MwClassEnum.ZOMBIE.getNiceName();
        }

        @Override
        public String getDescription(){
            return "Life is short!";
        }
    }

}
