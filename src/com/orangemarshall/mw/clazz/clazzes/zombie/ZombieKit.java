package com.orangemarshall.mw.clazz.clazzes.zombie;

import com.google.common.collect.Lists;
import com.orangemarshall.mw.clazz.Kit;
import com.orangemarshall.mw.util.format.LoreFormatter;
import com.orangemarshall.util.ItemStacks;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.List;


public class ZombieKit extends Kit{

    private ZombieSkillSetProvider zombieSkillSetProvider;

    public ZombieKit(ZombieSkillSetProvider zombieSkillSetProvider, int level){
        super(level);
        this.zombieSkillSetProvider = zombieSkillSetProvider;
    }

    @Override
    public List<ItemStack> getCopy(){
        List<ItemStack> items = Lists.newArrayList();

        ItemStack chestplate = ItemStacks.Builder.create(Material.DIAMOND_CHESTPLATE).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build();
        items.add(chestplate);

        String[] swordLore = new LoreFormatter().formatToArray(zombieSkillSetProvider.createDummy().getAbility().getDescription());
        Util.addPrefixToEach(swordLore, ChatColor.GRAY.toString());

        ItemStack ironSword = ItemStacks.Builder.create(Material.IRON_SWORD).setLore(swordLore).build();
        items.add(ironSword);

        return items;
    }

}
