package com.orangemarshall.mw.clazz.clazzes.zombie;

import com.google.common.collect.Sets;
import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;


public class WellTrainedGathering extends Passive{

    private Set<Material> typesToTrigger = Sets.newHashSet();
    private Random random = ThreadLocalRandom.current();

    public WellTrainedGathering(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);

        typesToTrigger.add(Material.COAL_ORE);
        typesToTrigger.add(Material.DIAMOND_ORE);
        typesToTrigger.add(Material.EMERALD_ORE);
        typesToTrigger.add(Material.GLOWING_REDSTONE_ORE);
        typesToTrigger.add(Material.GOLD_ORE);
        typesToTrigger.add(Material.IRON_ORE);
        typesToTrigger.add(Material.LAPIS_ORE);
        typesToTrigger.add(Material.QUARTZ_ORE);
        typesToTrigger.add(Material.REDSTONE_ORE);
        typesToTrigger.add(Material.LOG);
        typesToTrigger.add(Material.LOG_2);
        typesToTrigger.add(Material.WOOD);
    }

    @Override
    public String getName(){
        return "Well Trained";
    }

    @EventHandler
    public void onMine(BlockBreakEvent event){
        if(event.getPlayer() == mwPlayer.player){
            Material material = event.getBlock().getType();

            if(typesToTrigger.contains(material) && doesActivate()){
                activate();
            }
        }
    }

    private void activate(){
        mwPlayer.player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Util.secondsToTicks(20), 1));
    }

    private boolean doesActivate(){ //TODO
        return random.nextFloat()*10 < getActivationChance();
    }

    @Override
    public String getDescription(int level){
        return ChatColor.GREEN.toString() + getActivationChance(level) + "%" + ChatColor.GRAY + " chance to gain Haste II when mining or wood cutting, increasing speed you gather resources for 3.5 seconds.";
    }

    private float getActivationChance(){
        return getActivationChance(level);
    }

    private float getActivationChance(int level){
        return 5.0F + 3.75F*(level-1);
    }
}
