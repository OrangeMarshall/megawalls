package com.orangemarshall.mw.clazz.clazzes.zombie;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.mw.event.MwPlayerReceiveBowDamageEvent;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class BerserkPassive extends Passive{

    private Random random = ThreadLocalRandom.current();

    public BerserkPassive(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);
    }

    @Override
    public String getName(){
        return "Berserk";
    }

    @EventHandler
    public void onDamage(MwPlayerReceiveBowDamageEvent event){
        if(event.mwPlayer == mwPlayer){
            if(doesActivate()){
                activate();
            }
        }
    }

    private void activate(){
        Player player = mwPlayer.player;
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 3*20, 1));
        player.addPotionEffect(new PotionEffect(PotionEffectType.HARM, 3*20, 0));
    }

    private boolean doesActivate(){ //TODO
        return Util.shouldActivate(getActivationChance());
    }

    @Override
    public String getDescription(int level){
        return ChatColor.GRAY + "Getting hit by an arrow has a " + ChatColor.GREEN + getActivationChance(level) + "%" + ChatColor.GRAY + " chance to give you Strength I and Speed II for 3 seconds.";
    }

    private float getActivationChance(){
        return getActivationChance(level);
    }

    private float getActivationChance(int level){
        return 5.0F + 2.5F*(level-1);
    }

}
