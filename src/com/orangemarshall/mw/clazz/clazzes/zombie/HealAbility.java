package com.orangemarshall.mw.clazz.clazzes.zombie;

import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.PlayerAbility;
import com.orangemarshall.util.Particle;
import com.orangemarshall.util.UnicodeSymbol;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;


public class HealAbility extends PlayerAbility{

    public HealAbility(MwPlayer owner, int level){
        super(owner, level);
    }

    @Override
    public void register(){}

    @Override
    public void unregister(){}

    @Override
    public String getDescription(int level){
        return ChatColor.GRAY + "Heals you and surrounding teammates for " + ChatColor.GREEN + healAmount(level) + ChatColor.RED + UnicodeSymbol.HEART + ChatColor.GRAY + ".";
    }

    @Override
    public String getName(){
        return "Heal";
    }

    @Override
    public boolean attemptUse(){
        Player player = mwPlayer.player;

        double newHealth = healAmount() + player.getHealth();
        newHealth = Math.min(player.getMaxHealth(), newHealth);

        player.setHealth(newHealth);
        player.getWorld().playSound(player.getLocation(), Sound.LEVEL_UP, 1.0f, 1.0f);
        Particle.hearts(player.getLocation());

        return true;
    }

    private float healAmount(){
        return healAmount(level);
    }

    private float healAmount(int level){
        return 1f + (level-1)*0.25f;
    }

}
