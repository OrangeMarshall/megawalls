package com.orangemarshall.mw.clazz.clazzes.skeleton;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.MwClass;
import com.orangemarshall.mw.clazz.MwClassEnum;
import com.orangemarshall.mw.clazz.MwClassInformation;
import com.orangemarshall.mw.clazz.condition.LeftClickBowCondition;
import com.orangemarshall.mw.clazz.condition.RightClickSwordCondition;
import com.orangemarshall.mw.clazz.energy.BowEnergyListener;
import com.orangemarshall.mw.clazz.energy.EpsEnergyListener;
import com.orangemarshall.mw.clazz.energy.MeleeEnergyListener;


public final class Skeleton extends MwClass{

    public Skeleton(MwClassEnum classEnum, MegaWalls plugin, MwPlayer mwPlayer){
        super(classEnum, plugin, mwPlayer);
        this.mwPlayer = mwPlayer;

        this.abilityUseConditions.add(new RightClickSwordCondition(mwPlayer));
        this.abilityUseConditions.add(new LeftClickBowCondition(mwPlayer));

        this.energyListeners.add(new BowEnergyListener(plugin, mwPlayer, 20.0f));
        this.energyListeners.add(new MeleeEnergyListener(plugin, mwPlayer, 1.0f));
        this.energyListeners.add(new EpsEnergyListener(plugin, mwPlayer, 1.0f));
    }

    public static class Description extends MwClassInformation{

        @Override
        public String getName(){
            return MwClassEnum.SKELETON.getNiceName();
        }

        @Override
        public String getDescription(){
            return "Get sniped!";
        }
    }
}
