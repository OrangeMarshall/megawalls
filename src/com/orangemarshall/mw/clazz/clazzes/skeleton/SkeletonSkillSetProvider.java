package com.orangemarshall.mw.clazz.clazzes.skeleton;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.Kit;
import com.orangemarshall.mw.clazz.Upgrades;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.mw.clazz.skill.PlayerAbility;
import com.orangemarshall.mw.clazz.skill.SkillSet;
import com.orangemarshall.mw.clazz.skill.SkillSetProvider;


public class SkeletonSkillSetProvider implements SkillSetProvider{

    private static final Upgrades defaultUpgrades = new Upgrades(9, 9, 9, 9, 9);

    @Override
    public SkillSet create(MegaWalls plugin, MwPlayer mwPlayer){
        Upgrades upgrades = mwPlayer.getClassUpgrades();
        return getSkillSetForPlayer(plugin, mwPlayer, upgrades);
    }

    private SkillSet getSkillSetForPlayer(MegaWalls plugin, MwPlayer mwPlayer, Upgrades upgrades){
        PlayerAbility ability = new ExplosiveArrowAbility(mwPlayer, upgrades.getAbilityLevel());
        Passive passive1 = new SalvagingPassive(mwPlayer, upgrades.getPassive1Level(), plugin);
        Passive passive2 = new AgilePassive(mwPlayer,  upgrades.getPassive2Level(), plugin);
        Kit kit = new SkeletonKit(this, upgrades.getKitLevel());
        Passive gathering = new EfficiencyGathering(mwPlayer, upgrades.getGatheringLevel(), plugin);

        return new SkillSet(ability, passive1, passive2, kit, gathering);
    }

    @Override
    public SkillSet createDummy(){
        return getSkillSetForPlayer(null, null, defaultUpgrades);
    }

}
