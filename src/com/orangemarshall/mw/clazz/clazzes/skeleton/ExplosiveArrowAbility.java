package com.orangemarshall.mw.clazz.clazzes.skeleton;

import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.PlayerAbility;
import com.orangemarshall.mw.entity.ExplosiveArrow;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.entity.CreatureSpawnEvent;


public class ExplosiveArrowAbility extends PlayerAbility{

    public ExplosiveArrowAbility(MwPlayer owner, int level){
        super(owner, level);
    }

    @Override
    public void register(){}

    @Override
    public void unregister(){}

    @Override
    public String getDescription(int level){
        return ChatColor.GRAY + "Fire an explosive arrow that deals up to " + ChatColor.GREEN + damage(level) + ChatColor.GRAY + " damage to nearby players.";
    }

    @Override
    public String getName(){
        return "Explosive Arrow";
    }

    @Override
    public boolean attemptUse(){
        ExplosiveArrow arrow = new ExplosiveArrow(mwPlayer.player.getLocation(), damage(level));
        ((CraftWorld) mwPlayer.player.getWorld()).addEntity(arrow, CreatureSpawnEvent.SpawnReason.CUSTOM);
        return true;
    }

    private float damage(int level){
        return 4.0f + (level-1)*0.5f;
    }

}
