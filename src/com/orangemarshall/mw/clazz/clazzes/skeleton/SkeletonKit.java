package com.orangemarshall.mw.clazz.clazzes.skeleton;

import com.google.common.collect.Lists;
import com.orangemarshall.mw.clazz.Kit;
import com.orangemarshall.mw.util.format.LoreFormatter;
import com.orangemarshall.util.ItemStacks;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.List;


public class SkeletonKit extends Kit{

    private SkeletonSkillSetProvider skeletonSkillSetProvider;

    public SkeletonKit(SkeletonSkillSetProvider skeletonSkillSetProvider, int level){
        super(level);
        this.skeletonSkillSetProvider = skeletonSkillSetProvider;
    }

    @Override
    public List<ItemStack> getCopy(){
        List<ItemStack> items = Lists.newArrayList();

        ItemStack helmet = ItemStacks.Builder.create(Material.DIAMOND_HELMET)
                .addEnchantment(Enchantment.PROTECTION_PROJECTILE, 3)
                .build();
        items.add(helmet);

        String[] swordLore = new LoreFormatter().formatToArray(skeletonSkillSetProvider.createDummy().getAbility().getDescription());
        Util.addPrefixToEach(swordLore, ChatColor.GRAY.toString());

        ItemStack ironSword = ItemStacks.Builder.create(Material.IRON_SWORD)
                .addEnchantment(Enchantment.DURABILITY, 3)
                .setLore(swordLore)
                .build();
        items.add(ironSword);

        ItemStack ironAxe = ItemStacks.Builder.create(Material.IRON_AXE)
                .addEnchantment(Enchantment.DURABILITY, 3)
                .addEnchantment(Enchantment.DIG_SPEED, 2)
                .build();
        items.add(ironAxe);

        ItemStack bow = ItemStacks.Builder.create(Material.BOW)
                .addEnchantment(Enchantment.ARROW_DAMAGE, 3)
                .addEnchantment(Enchantment.DURABILITY, 1)
                .build();
        items.add(bow);

        ItemStack arrows = ItemStacks.Builder.create(Material.ARROW).setAmount(64).build();
        items.add(arrows);

        return items;
    }

}
