package com.orangemarshall.mw.clazz.clazzes.skeleton;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.mw.event.MwPlayerDealBowDamageEvent;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;


public class SalvagingPassive extends Passive{

    public SalvagingPassive(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);
    }

    @Override
    public String getName(){
        return "Salvaging";
    }

    @EventHandler
    public void onKill(MwPlayerDealBowDamageEvent event){
        if(event.mwPlayer == mwPlayer){
            if(doesActivate()){
                activate();
            }
        }
    }

    private void activate(){
        mwPlayer.player.getInventory().addItem(new ItemStack(Material.ARROW, 2));
        mwPlayer.player.setFoodLevel(mwPlayer.player.getFoodLevel() + 2);
    }

    private boolean doesActivate(){
        return Util.shouldActivate(getActivationChance(level));
    }

    @Override
    public String getDescription(int level){
        return ChatColor.GREEN.toString() + getActivationChance(level) + "% " + ChatColor.GRAY + " chance to have two arrows and saturation given back when shooting an enemy.";
    }

    private float getActivationChance(int level){
        return 10.0F + 10.0F*(level-1);
    }

}
