package com.orangemarshall.mw.clazz.clazzes.skeleton;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.mw.event.MwPlayerDealBowDamageEvent;
import com.orangemarshall.util.Cooldown;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.concurrent.TimeUnit;


public class AgilePassive extends Passive{

    private Cooldown cooldown;

    public AgilePassive(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);

        int duration = (int) (getDuration(level)*1000);
        cooldown = new Cooldown(duration, TimeUnit.MILLISECONDS);
    }

    @Override
    public String getName(){
        return "Agile";
    }

    @EventHandler
    public void onDamage(MwPlayerDealBowDamageEvent event){
        if(event.mwPlayer == mwPlayer){
            if(cooldown.attemptReset()){
                activate(event.mwPlayer.player);
            }
        }
    }

    private void activate(Player player){
        float duration = getDuration(level);

        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Util.secondsToTicks(duration), 1));
        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Util.secondsToTicks(duration), 0));
    }

    @Override
    public String getDescription(int level){
        float duration = getDuration(level);
        return ChatColor.GRAY + "Gain Speed II + Regeneration I for " + ChatColor.GREEN + duration + ChatColor.GRAY + " seconds when you hit an enemy with your bow. Cooldown " + ChatColor.GREEN + duration + ChatColor.GRAY + "s.";
    }

    private float getDuration(int level){
        return 3 + (level-1)*0.5f;
    }

}
