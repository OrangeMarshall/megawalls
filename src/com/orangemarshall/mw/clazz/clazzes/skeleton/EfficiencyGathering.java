package com.orangemarshall.mw.clazz.clazzes.skeleton;

import com.google.common.collect.Sets;
import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.skill.Passive;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.Set;


public class EfficiencyGathering extends Passive{

    private Set<Material> typesToTrigger = Sets.newHashSet();

    public EfficiencyGathering(MwPlayer mwPlayer, int level, MegaWalls plugin){
        super(mwPlayer, level, plugin);

        typesToTrigger.add(Material.COAL_ORE);
        typesToTrigger.add(Material.DIAMOND_ORE);
        typesToTrigger.add(Material.EMERALD_ORE);
        typesToTrigger.add(Material.GLOWING_REDSTONE_ORE);
        typesToTrigger.add(Material.GOLD_ORE);
        typesToTrigger.add(Material.IRON_ORE);
        typesToTrigger.add(Material.LAPIS_ORE);
        typesToTrigger.add(Material.QUARTZ_ORE);
        typesToTrigger.add(Material.REDSTONE_ORE);
        typesToTrigger.add(Material.STONE);
        typesToTrigger.add(Material.LOG);
        typesToTrigger.add(Material.LOG_2);
        typesToTrigger.add(Material.WOOD);
    }

    @Override
    public String getName(){
        return "Efficiency";
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onMine(BlockBreakEvent event){
        if(event.getPlayer() == mwPlayer.player){
            Material material = event.getBlock().getType();

            if(typesToTrigger.contains(material) && doesActivate()){
                Location location = event.getBlock().getLocation();

                Material type = event.getBlock().getType();
                byte data = event.getBlock().getData();

                event.getBlock().breakNaturally();

                location.getBlock().setType(type);
                location.getBlock().setData(data);

                location.getBlock().breakNaturally();

                event.setCancelled(true);

                //TODO check for tools used
                //maybe drop item then duplicate?
            }
        }
    }

    private boolean doesActivate(){
        return Util.shouldActivate(getActivationChance());
    }

    @Override
    public String getDescription(int level){
        return ChatColor.GREEN.toString() + getActivationChance(level) + "%" + ChatColor.GRAY + " chance to get Regeneration II for 5 seconds when you mine ores.";
    }

    private float getActivationChance(){
        return getActivationChance(level);
    }

    private float getActivationChance(int level){
        return 50.0F + 6.25F*(level-1);
    }
}
