package com.orangemarshall.mw.clazz;

import com.orangemarshall.mw.clazz.clazzes.blaze.Blaze;
import com.orangemarshall.mw.clazz.clazzes.blaze.BlazeSkillSetProvider;
import com.orangemarshall.mw.clazz.clazzes.skeleton.Skeleton;
import com.orangemarshall.mw.clazz.clazzes.skeleton.SkeletonSkillSetProvider;
import com.orangemarshall.mw.clazz.clazzes.zombie.Zombie;
import com.orangemarshall.mw.clazz.clazzes.zombie.ZombieSkillSetProvider;
import com.orangemarshall.mw.clazz.skill.SkillSetProvider;
import com.orangemarshall.util.ItemStacks;
import org.apache.commons.lang.WordUtils;
import org.bukkit.inventory.ItemStack;


public enum MwClassEnum{

    PIGMAN(ClassType.HERO, SkinEnum.PIGMAN, new Zombie.Description(), new ZombieSkillSetProvider()),
    SHAMAN(ClassType.HERO, SkinEnum.SHAMAN, new Zombie.Description(), new ZombieSkillSetProvider()),
    SKELETON(ClassType.NORMAL, SkinEnum.SKELETON, new Skeleton.Description(), new SkeletonSkillSetProvider()),
    SPIDER(ClassType.NORMAL, SkinEnum.SPIDER, new Zombie.Description(), new ZombieSkillSetProvider()),
    ZOMBIE(ClassType.NORMAL, SkinEnum.ZOMBIE, new Zombie.Description(), new ZombieSkillSetProvider()),
    GOLEM(ClassType.HERO, SkinEnum.GOLEM, new Zombie.Description(), new ZombieSkillSetProvider()),
    HEROBRINE(ClassType.NORMAL, SkinEnum.HEROBRINE, new Zombie.Description(), new ZombieSkillSetProvider()),
    DREADLORD(ClassType.HERO, SkinEnum.DREADLORD, new Zombie.Description(), new ZombieSkillSetProvider()),
    CREEPER(ClassType.NORMAL, SkinEnum.CREEPER, new Zombie.Description(), new ZombieSkillSetProvider()),
    ENDERMAN(ClassType.NORMAL, SkinEnum.ENDERMAN, new Zombie.Description(), new ZombieSkillSetProvider()),
    ARCANIST(ClassType.HERO, SkinEnum.ARCANIST, new Zombie.Description(), new ZombieSkillSetProvider()),
    BLAZE(ClassType.HERO, SkinEnum.BLAZE, new Blaze.Description(), new BlazeSkillSetProvider()),
    SQUID(ClassType.HERO, SkinEnum.SQUID, new Zombie.Description(), new ZombieSkillSetProvider()),
    HUNTER(ClassType.HERO, SkinEnum.HUNTER, new Zombie.Description(), new ZombieSkillSetProvider()),
    PIRATE(ClassType.HERO, SkinEnum.PIRATE, new Zombie.Description(), new ZombieSkillSetProvider()),
    PHOENIX(ClassType.MYTHIC, SkinEnum.PHOENIX, new Zombie.Description(), new ZombieSkillSetProvider()),
    MOLEMAN(ClassType.MYTHIC, SkinEnum.PHOENIX, new Zombie.Description(), new ZombieSkillSetProvider()),
    WEREWOLF(ClassType.MYTHIC, SkinEnum.PHOENIX, new Zombie.Description(), new ZombieSkillSetProvider());


    private SkinEnum skin;
    private ClassType classType;
    private Descriptable classInformation;
    private SkillSetProvider skillSetProvider;

    MwClassEnum(ClassType classType, SkinEnum skin, Descriptable classInformation, SkillSetProvider skillSetProvider){
        this.skin = skin;
        this.classType = classType;
        this.classInformation = classInformation;
        this.skillSetProvider = skillSetProvider;
    }

    public SkillSetProvider getSkillSetProvider(){
        return skillSetProvider;
    }

    public Descriptable getInformation(){
        return classInformation;
    }

    public String getNiceName(){
        return WordUtils.capitalizeFully(name());
    }

    public SkinEnum getSkin(){
        return skin;
    }

    public ItemStack getSkull(){
        return ItemStacks.getSkullWithSkin(getSkin().getSkinUrl());
    }

    public ClassType getClassType(){
        return classType;
    }

}
