package com.orangemarshall.mw.clazz;

import com.google.common.collect.Lists;
import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.util.MwUtil;
import com.orangemarshall.mw.clazz.condition.AbilityUseCondition;
import com.orangemarshall.mw.clazz.energy.EnergyListener;
import com.orangemarshall.mw.clazz.skill.SkillSet;
import com.orangemarshall.util.Notifier;
import com.orangemarshall.mw.util.PluginHolder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;


public abstract class MwClass extends PluginHolder {

    protected MwPlayer mwPlayer;

    protected List<AbilityUseCondition> abilityUseConditions = Lists.newArrayList();
    protected List<EnergyListener> energyListeners = Lists.newArrayList();

    private SkillSet skills;
    private MwClassEnum classEnum;

    public MwClass(MwClassEnum classEnum, MegaWalls plugin, MwPlayer mwPlayer){
        super(plugin);
        this.mwPlayer = mwPlayer;
        this.classEnum = classEnum;
        this.skills = classEnum.getSkillSetProvider().create(plugin, mwPlayer);
    }

    public SkillSet getSkills(){
        return skills;
    }

    public MwClassEnum getClassEnum(){
        return classEnum;
    }

    public MwPlayer getMwPlayer(){
        return mwPlayer;
    }

    public void die(){
        energyListeners.forEach(EnergyListener::unregister);
        skills.unregisterAll();
    }

    public void spawn(){    //TODO move somewhere else?
        Player player = mwPlayer.player;

        String message = ChatColor.GREEN + "Spawning you as a " + ChatColor.YELLOW + classEnum.getNiceName() + ChatColor.GREEN + "!";
        new Notifier().toPlayer(player).send(message);

        MwUtil.removeItems(player, MwUtil::isKitItem);

        List<ItemStack> itemStacks = skills.getKit().getCopy();
        MwUtil.equipItems(player, itemStacks);
        player.updateInventory();

        energyListeners.forEach(EnergyListener::register);

        skills.registerAll();
    }

    public boolean attemptAbilityUse(AbilityUseCondition.ClickType clickType, Material heldItem){
        for(AbilityUseCondition condition : abilityUseConditions){
            if(condition.attemptUse(clickType, heldItem)){
                if(skills.getAbility().attemptUse()){
                    mwPlayer.addEnergy(-100f);
                }
                return true;
            }
        }
        return false;
    }

}
