package com.orangemarshall.mw.clazz;

@FunctionalInterface
public interface TieredDescription{

    String getDescription(int level);

}
