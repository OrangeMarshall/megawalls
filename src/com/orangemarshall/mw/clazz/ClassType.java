package com.orangemarshall.mw.clazz;

import org.apache.commons.lang.WordUtils;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public enum ClassType{
    NORMAL, HERO, MYTHIC;

    public Collection<MwClassEnum> getClassesOfType(){
        return Stream.of(MwClassEnum.values())
                .filter(classEnum -> classEnum.getClassType() == this)
                .collect(Collectors.toList());
    }

    public String getNiceName(){
        return WordUtils.capitalizeFully(name());
    }

}
