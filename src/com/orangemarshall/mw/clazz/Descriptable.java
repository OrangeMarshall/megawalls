package com.orangemarshall.mw.clazz;

public interface Descriptable{

    String getName();
    String getDescription();

}
