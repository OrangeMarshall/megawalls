package com.orangemarshall.mw.entity;

import com.google.common.collect.Sets;
import com.orangemarshall.mw.entity.pathfinder.PathfinderGoalNearestHostilePlayer;
import net.minecraft.server.v1_8_R3.EntityBlaze;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import net.minecraft.server.v1_8_R3.LocaleI18n;
import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public class FriendlyBlaze extends EntityBlaze implements FriendlyEntity{

    private Collection<net.minecraft.server.v1_8_R3.Entity> friendly = Sets.newHashSet();

    public FriendlyBlaze(org.bukkit.World world){
        super(((CraftWorld) world).getHandle());

        this.targetSelector = new PathfinderGoalSelector(((CraftWorld) world).getHandle().methodProfiler);
        this.targetSelector.a(1, new PathfinderGoalNearestHostilePlayer(this, friendly));
    }

    public String getName(){
        if(this.hasCustomName()){
            return this.getCustomName();
        }else{
            return LocaleI18n.get("entity.Blaze.name");
        }
    }

    @Override
    public void addFriendly(Entity... entities){
        for(Entity entity : entities){
            friendly.add(((CraftEntity) entity).getHandle());
        }
    }

    @Override
    public List<Entity> getFriendly(){
        return friendly.stream().map(e -> e.getBukkitEntity()).collect(Collectors.toList());
    }

    @Override
    protected void initAttributes(){
        super.initAttributes();
        this.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(0.0D);
        this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.5D);
        this.getAttributeInstance(GenericAttributes.FOLLOW_RANGE).setValue(48.0D);
    }

}
