package com.orangemarshall.mw.entity;

import com.orangemarshall.util.FieldWrapper;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.lang.reflect.Method;
import java.util.Map;


public enum MwEntityRegistry{

    CUSTOM_ZOMBIE(EntityZombie.class, CustomZombie.class),
    FRIENDLY_BLAZE(EntityBlaze.class, FriendlyBlaze.class),
    EXPLOSIVE_ARROW(EntityArrow.class, ExplosiveArrow.class);

    private String name;
    private Class<? extends Entity> nmsClass;
    private Class<? extends Entity> customClass;

    MwEntityRegistry(Class<? extends Entity> nmsClass, Class<? extends Entity> customClass) {
        this.name = name();
        this.nmsClass = nmsClass;
        this.customClass = customClass;
    }

    public String getName() {
        return name;
    }

    public Class<? extends Entity> getNmsClass() {
        return nmsClass;
    }

    public Class<? extends Entity> getCustomClass() {
        return customClass;
    }

    public static <T extends Entity> CraftEntity spawn(T entity, Location location){
        World nmsWorld = ((CraftWorld) location.getWorld()).getHandle();

        entity.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
        nmsWorld.addEntity(entity, CreatureSpawnEvent.SpawnReason.CUSTOM);

        return entity.getBukkitEntity();
    }

    public static MwEntityRegistry byCustomClass(Class<? extends Entity> customClass){
        for(MwEntityRegistry type : MwEntityRegistry.values()){
            if(customClass == type.getCustomClass()){
                return type;
            }
        }

        throw new IllegalArgumentException("Did not find custom class for " + customClass);
    }


    private static final FieldWrapper<Map<Class<? extends Entity>, Integer>> entityClassToIdField = new FieldWrapper<>("f", EntityTypes.class);

    private static void registerEntityClass(String name, Class<? extends Entity> customClass, Class<? extends Entity> nmsBaseClass) throws ReflectiveOperationException{
        Map<Class<? extends Entity>, Integer> entityClassToId = entityClassToIdField.get(null);

        int byteOverflow = Byte.MAX_VALUE + 1;

        int nmsClassId = entityClassToId.get(nmsBaseClass);
        int convertedId = nmsClassId + byteOverflow*2;

        while(entityClassToId.containsValue(convertedId)){
            convertedId += byteOverflow;
        }

        Method method = EntityTypes.class.getDeclaredMethod("a", Class.class, String.class, int.class);
        method.setAccessible(true);
        method.invoke(null, customClass, name, convertedId);
    }

    public static void registerType(MwEntityRegistry entity){
        try {
            registerEntityClass(entity.getName(), entity.getCustomClass(), entity.getNmsClass());
        } catch (Exception e) {
            throw new IllegalStateException("Could not register custom entity " + entity + "!", e);
        }
    }

    public static void registerAll(){
        for(MwEntityRegistry entity : MwEntityRegistry.values()){
            registerType(entity);
        }
    }
}
