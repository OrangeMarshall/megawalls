package com.orangemarshall.mw.entity.pathfinder;

import com.google.common.base.Predicates;
import com.orangemarshall.util.FieldWrapper;
import net.minecraft.server.v1_8_R3.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class PathfinderGoalNearestHostilePlayer<T> extends PathfinderGoalNearestAttackableTarget {

    private static final FieldWrapper<Integer> g = new FieldWrapper<>("g", PathfinderGoalNearestAttackableTarget.class);

    private Collection<Entity> friendly;

    public PathfinderGoalNearestHostilePlayer(EntityCreature entitycreature, Collection<Entity> friendly){
        super(entitycreature, EntityHuman.class, true);
        this.friendly = friendly;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean a() {
        if(g.get(this) > 0 && this.e.bc().nextInt(g.get(this)) != 0) {
            return false;
        } else {
            double d0 = this.f();
            List list = this.e.world.a(this.a, this.e.getBoundingBox().grow(d0, 4.0D, d0), Predicates.and(this.c, IEntitySelector.d));
            Collections.sort(list, this.b);

            list.removeAll(friendly);

            if(list.isEmpty()) {
                return false;
            } else {
                this.d = (EntityLiving)list.get(0);
                return true;
            }
        }
    }
}
