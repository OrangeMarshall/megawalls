package com.orangemarshall.mw.entity;

import com.google.common.collect.Lists;
import com.orangemarshall.mw.util.FriendlyExplosion;
import net.minecraft.server.v1_8_R3.EntityArrow;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public class ExplosiveArrow extends EntityArrow implements FriendlyEntity{

    private List<net.minecraft.server.v1_8_R3.Entity> friendly = Lists.newArrayList();

    private Location location;

    private double maxDmg;

    public ExplosiveArrow(Location location, double dmg){
        super(((CraftWorld)location.getWorld()).getHandle());
        this.location = location;
        this.maxDmg = dmg;
    }

    @Override
    public void t_(){
        super.t_();

        if(this.isInGround()){
            castExplosion();
        }
    }

    public void setMaxExplosionDamage(double dmg){
        this.maxDmg = dmg;
    }

    private void castExplosion(){
        int size = 4;   //TODO add dmg

        FriendlyExplosion explosion = new FriendlyExplosion(location, size, false, true, friendly);
        explosion.a();
        explosion.a(true);
    }

    @Override
    public void addFriendly(Entity... entities){
        for(Entity entity : entities){
            friendly.add(((CraftEntity) entity).getHandle());
        }
    }

    @Override
    public Collection<Entity> getFriendly(){
        return friendly.stream().map(net.minecraft.server.v1_8_R3.Entity::getBukkitEntity).collect(Collectors.toList());
    }

}
