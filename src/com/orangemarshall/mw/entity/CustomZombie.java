package com.orangemarshall.mw.entity;

import net.minecraft.server.v1_8_R3.EntityZombie;
import net.minecraft.server.v1_8_R3.LocaleI18n;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;


public class CustomZombie extends EntityZombie {

    public CustomZombie(World world){
        super(((CraftWorld) world).getHandle());
    }

    public String getName(){
        if(this.hasCustomName()){
            return this.getCustomName();
        }else{
            return LocaleI18n.get("entity.Zombie.name");
        }
    }

}
