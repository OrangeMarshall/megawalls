package com.orangemarshall.mw.entity;

import org.bukkit.entity.Entity;

import java.util.Collection;


public interface FriendlyEntity{

    void addFriendly(Entity... entities);
    Collection<Entity> getFriendly();

}
