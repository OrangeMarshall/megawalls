package com.orangemarshall.mw.listener;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.util.SimpleListener;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;


public class AutoRespawnListener extends SimpleListener{

    public AutoRespawnListener(MegaWalls plugin){
        super(plugin);
    }

    @EventHandler
    public void onBeforeRespawn(PlayerDeathEvent event){
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            PacketPlayInClientCommand packet = new PacketPlayInClientCommand(PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN);
            ((CraftPlayer) event.getEntity()).getHandle().playerConnection.a(packet);
        }, 20L);
    }

}
