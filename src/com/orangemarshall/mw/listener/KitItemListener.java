package com.orangemarshall.mw.listener;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.util.MwUtil;
import com.orangemarshall.mw.util.SimpleListener;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;


public class KitItemListener extends SimpleListener{

    public KitItemListener(MegaWalls plugin){
        super(plugin);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event){
        if(MwUtil.isKitItem(event.getItemDrop().getItemStack())){
            event.setCancelled(true);
        }
    }

    //Don't let people pick up kit items and despawn them
    @EventHandler
    public void onPickup(PlayerPickupItemEvent event){
        ItemStack is = event.getItem().getItemStack();
        if(MwUtil.isKitItem(is)){
            event.setCancelled(true);
            event.getItem().remove();
        }
    }

    //Don't let people allow to collect kit items from inventories (e.g. chests)
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        ItemStack is = event.getCurrentItem();
        if(MwUtil.isKitItem(is)){
            if(event.getInventory().getType() != InventoryType.PLAYER && event.getInventory().getType() != InventoryType.WORKBENCH && event.getInventory().getType() != InventoryType.CRAFTING){
                event.setCancelled(true);
                event.getWhoClicked().sendMessage(ChatColor.RED + "You're not allowed to move that in this inventory!");
            }
        }
    }

}
