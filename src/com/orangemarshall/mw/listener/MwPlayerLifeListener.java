package com.orangemarshall.mw.listener;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.util.MwUtil;
import com.orangemarshall.mw.event.MwPlayerChangeClassEvent;
import com.orangemarshall.mw.event.MwPlayerDeathEvent;
import com.orangemarshall.mw.event.MwPlayerSpawnEvent;
import com.orangemarshall.mw.util.SimpleListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;


public class MwPlayerLifeListener extends SimpleListener{

    public MwPlayerLifeListener(MegaWalls plugin){
        super(plugin);
    }

    @EventHandler
    public void onChangeClass(MwPlayerChangeClassEvent event){
        event.mwPlayer.die();
        event.mwPlayer.player.setHealth(0.0f);
    }

    @EventHandler
    public void onSpawn(MwPlayerSpawnEvent event){
        plugin.getServer().getScheduler().runTaskLater(plugin, event.mwPlayer::spawn, 1);
    }

    @EventHandler
    public void onDeath(MwPlayerDeathEvent event){
        event.mwPlayer.die();
    }

    @EventHandler
    public void onDropItems(PlayerDeathEvent event){
        event.getDrops().removeIf(MwUtil::isKitItem);
    }

}
