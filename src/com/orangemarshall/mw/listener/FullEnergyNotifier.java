package com.orangemarshall.mw.listener;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.util.SimpleListener;
import com.orangemarshall.util.Util;
import org.bukkit.ChatColor;


public class FullEnergyNotifier extends SimpleListener{

    private int taskId = -1;

    public FullEnergyNotifier(MegaWalls plugin){
        super(plugin);
    }

    @Override
    public void register(){
        if(taskId < 0){
            plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new NotifierRunnable(), 20L, Util.secondsToTicks(8));
        }
    }

    @Override
    public void unregister(){
        if(taskId >= 0){
            plugin.getServer().getScheduler().cancelTask(taskId);
        }
    }

    private class NotifierRunnable implements Runnable{

        @Override
        public void run(){
            for(MwPlayer mwPlayer : plugin.getMwPlayerRegistry().getOnlinePlayers()){
                if(mwPlayer.getEnergy() == 100.0F){
                    notifyPlayer(mwPlayer);
                }
            }
        }

        private void notifyPlayer(MwPlayer mwPlayer){
            String abilityName = mwPlayer.getMwClassInstance().getSkills().getAbility().getName();

            String message = ChatColor.GREEN + "Your " + ChatColor.AQUA + ChatColor.BOLD + abilityName + ChatColor.GREEN + " Skill is ready!\n"
                            + ChatColor.AQUA + "Right click " + ChatColor.GREEN + "a sword or "+ChatColor.AQUA+"left click "+ChatColor.GREEN+"a bow to use your skill!";

            mwPlayer.player.sendMessage(message);
        }

    }

}
