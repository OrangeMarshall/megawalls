package com.orangemarshall.mw.listener;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.event.MwPlayerDeathEvent;
import com.orangemarshall.mw.event.MwPlayerSpawnEvent;
import com.orangemarshall.mw.util.SimpleListener;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;


public class ConnectionListener extends SimpleListener{

    public ConnectionListener(MegaWalls plugin){
        super(plugin);
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            Bukkit.getOnlinePlayers().forEach(player -> {
                MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate(player);
                plugin.callEvent(new MwPlayerSpawnEvent(mwPlayer));
            });
        }, 1L);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onConnect(PlayerJoinEvent event){
        event.getPlayer().getInventory().clear();

        MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate(event.getPlayer());
        plugin.callEvent(new MwPlayerSpawnEvent(mwPlayer));
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event){
        MwPlayer mwPlayer = plugin.getMwPlayerRegistry().removePlayer(event.getPlayer());
        plugin.callEvent(new MwPlayerDeathEvent(mwPlayer));
    }

}
