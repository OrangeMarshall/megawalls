package com.orangemarshall.mw.listener;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.entity.FriendlyEntity;
import com.orangemarshall.mw.util.SimpleListener;
import com.orangemarshall.util.FieldWrapper;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityBlaze;
import net.minecraft.server.v1_8_R3.PathfinderGoal;
import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftBlaze;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.lang.reflect.Field;
import java.util.List;


public class EntityListener extends SimpleListener{

    public EntityListener(MegaWalls plugin){
        super(plugin);
    }

    private static final FieldWrapper<List<Object>> goals = new FieldWrapper<>("b", PathfinderGoalSelector.class);

    @EventHandler
    public void onFireballHit(EntityDamageByEntityEvent event){
        try{
            if(event.getDamager() instanceof Projectile){
                Projectile projectile = (Projectile) event.getDamager();

                if(event.getEntity() == projectile.getShooter()){
                    event.setCancelled(true);
                }else if(projectile.getShooter() instanceof Blaze){
                    EntityBlaze blaze = ((CraftBlaze) projectile.getShooter()).getHandle();

                    for(Object o : goals.get(blaze.targetSelector)){
                        //TODO cache
                        //TODO make custom entity instead
                        Field field = o.getClass().getDeclaredField("a");
                        field.setAccessible(true);
                        PathfinderGoal goal = (PathfinderGoal) field.get(o);

                        if(goal instanceof FriendlyEntity){
                            FriendlyEntity friendly = (FriendlyEntity) goal;
                            Entity nmsEntity = ((CraftEntity) event.getEntity()).getHandle();

                            if(friendly.getFriendly().contains(nmsEntity)){
                                event.setCancelled(true);
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
