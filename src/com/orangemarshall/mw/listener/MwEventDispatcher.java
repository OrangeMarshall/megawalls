package com.orangemarshall.mw.listener;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.condition.AbilityUseCondition;
import com.orangemarshall.mw.event.*;
import com.orangemarshall.mw.util.SimpleListener;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;


public class MwEventDispatcher extends SimpleListener{

    public MwEventDispatcher(MegaWalls plugin){
        super(plugin);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onDamage(EntityDamageByEntityEvent event){
        Entity entity = event.getEntity();
        Entity damager = event.getDamager();

        if(entity instanceof Player && damager instanceof Player){
            MwPlayer mwDamager = plugin.getMwPlayerRegistry().getOrCreate((Player) damager);
            MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate((Player) entity);

            plugin.callEvent(new MwPlayerDealMeleeDamageEvent(mwDamager, event));
            plugin.callEvent(new MwPlayerReceiveMeleeDamageEvent(mwPlayer, event));
        }else if(entity instanceof Player && damager instanceof Arrow){
            MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate((Player) entity);
            Arrow arrow = (Arrow) damager;

            plugin.callEvent(new MwPlayerReceiveBowDamageEvent(mwPlayer, event));

            if(arrow.getShooter() instanceof Player){
                MwPlayer shooter = plugin.getMwPlayerRegistry().getOrCreate((Player) arrow.getShooter());
                plugin.callEvent(new MwPlayerDealBowDamageEvent(shooter, event));
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event){
        MwPlayer playerDead = plugin.getMwPlayerRegistry().getOrCreate(event.getEntity());
        plugin.callEvent(new MwPlayerDeathEvent(playerDead));

        Player killer = event.getEntity().getKiller();
        if(killer != null){
            MwPlayer mwKiller = plugin.getMwPlayerRegistry().getOrCreate(killer);
            plugin.callEvent(new MwPlayerKillEvent(mwKiller));
        }


    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event){
        MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate(event.getPlayer());
        plugin.callEvent(new MwPlayerSpawnEvent(mwPlayer));
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onInteract(PlayerInteractEvent event){
        if(event.getAction() != Action.PHYSICAL){
            Player player = event.getPlayer();
            MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate(player);

            AbilityUseCondition.ClickType clickType = toClickType(event.getAction());
            Material heldItem = player.getItemInHand().getType();

            mwPlayer.getMwClassInstance().attemptAbilityUse(clickType, heldItem);
        }
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onInteract(PlayerInteractAtEntityEvent event){
        Player player = event.getPlayer();
        MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate(player);

        Material heldItem = player.getItemInHand().getType();

        mwPlayer.getMwClassInstance().attemptAbilityUse(AbilityUseCondition.ClickType.RIGHT, heldItem);
    }

    private AbilityUseCondition.ClickType toClickType(Action action){
        switch(action){
            case RIGHT_CLICK_BLOCK:
            case RIGHT_CLICK_AIR:
                return AbilityUseCondition.ClickType.RIGHT;
            case LEFT_CLICK_BLOCK:
            case LEFT_CLICK_AIR:
                return AbilityUseCondition.ClickType.LEFT;
            default:
                throw new IllegalArgumentException("Could not find Clicktype matching " + action.name());
        }
    }

}
