package com.orangemarshall.mw;

import com.orangemarshall.mw.clazz.MwClass;
import com.orangemarshall.mw.clazz.MwClassEnum;
import com.orangemarshall.mw.clazz.clazzes.blaze.Blaze;
import com.orangemarshall.mw.clazz.clazzes.skeleton.Skeleton;
import com.orangemarshall.mw.clazz.skill.SkillSet;
import com.orangemarshall.mw.clazz.clazzes.zombie.Zombie;


public class MwFactory{

    public static MwClass createClassInstance(MwClassEnum clazz, MegaWalls plugin, MwPlayer mwPlayer){
        switch(clazz){
            case PIGMAN:
            case SHAMAN:
            case SPIDER:
            case ZOMBIE:
            case HEROBRINE:
            case GOLEM:
            case DREADLORD:
            case CREEPER:
            case ENDERMAN:
            case ARCANIST:
            case SQUID:
            case HUNTER:
            case PIRATE:
            case PHOENIX:
            case MOLEMAN:
            case WEREWOLF:
                return new Zombie(clazz, plugin, mwPlayer);
            case BLAZE:
                return new Blaze(clazz, plugin, mwPlayer);
            case SKELETON:
                return new Skeleton(clazz, plugin, mwPlayer);

            default: throw new IllegalArgumentException("Could not find case for type " + clazz);
        }
    }

    public static SkillSet createDummySkillSetInstance(MwClassEnum clazz){
        return clazz.getSkillSetProvider().createDummy();
    }

}
