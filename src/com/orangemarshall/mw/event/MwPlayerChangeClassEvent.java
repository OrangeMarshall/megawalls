package com.orangemarshall.mw.event;

import com.orangemarshall.mw.MwPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class MwPlayerChangeClassEvent extends Event{

    private static final HandlerList handlers = new HandlerList();

    public final MwPlayer mwPlayer;

    //TODO death reason + msg
    public MwPlayerChangeClassEvent(MwPlayer mwPlayer){
        this.mwPlayer = mwPlayer;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }

}
