package com.orangemarshall.mw.event;

import com.orangemarshall.mw.MwPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageByEntityEvent;


public class MwPlayerDealBowDamageEvent extends Event{

    private static final HandlerList handlers = new HandlerList();

    public final MwPlayer mwPlayer;
    public final EntityDamageByEntityEvent event;

    public MwPlayerDealBowDamageEvent(MwPlayer mwPlayer, EntityDamageByEntityEvent event){
        this.mwPlayer = mwPlayer;
        this.event = event;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }

}
