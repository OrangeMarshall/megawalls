package com.orangemarshall.mw.event;

import com.orangemarshall.mw.MwPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageByEntityEvent;


public class MwPlayerDealMeleeDamageEvent extends Event{

    private static final HandlerList handlers = new HandlerList();

    public final EntityDamageByEntityEvent event;
    public final MwPlayer mwPlayer;

    public MwPlayerDealMeleeDamageEvent(MwPlayer mwPlayer, EntityDamageByEntityEvent event){
        this.event = event;
        this.mwPlayer = mwPlayer;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }

}
