package com.orangemarshall.mw;

import com.orangemarshall.mw.clazz.energy.EnergyVisualizerListener;
import com.orangemarshall.mw.command.CommandKill;
import com.orangemarshall.mw.command.CommandShop;
import com.orangemarshall.mw.command.CommandSurface;
import com.orangemarshall.mw.entity.MwEntityRegistry;
import com.orangemarshall.mw.event.MwPlayerChangeClassEvent;
import com.orangemarshall.mw.listener.*;
import com.orangemarshall.mw.util.WorldConfigListener;
import com.orangemarshall.mw.util.WorldConfiguration;
import com.orangemarshall.mw.util.skin.SkinListener;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.event.Event;
import org.bukkit.plugin.java.JavaPlugin;


public class MegaWalls extends JavaPlugin{

    private MwPlayerRegistry mwPlayerRegistry;
    private WorldConfiguration worldConfig;

    //TODO disable /minecraft:cmd

    //TODO unregister entities on disable

    //TODO on login you keep prev class items??

    @Override
    public void onEnable(){
        MwEntityRegistry.registerAll();

        new CommandShop(this).enable();
        new CommandKill(this).enable();
        new CommandSurface(this).enable();

        new MwPlayerLifeListener(this).register();
        new ConnectionListener(this).register();
        new MwEventDispatcher(this).register();

        new EnergyVisualizerListener(this).register();
        new KitItemListener(this).register();

        new AutoRespawnListener(this).register();
        new FullEnergyNotifier(this).register();

        new EntityListener(this).register();

        worldConfig = new WorldConfigListener(this);
        ((WorldConfigListener)worldConfig).register();

        mwPlayerRegistry = new MwPlayerRegistry(this);

        Bukkit.getScheduler().scheduleSyncDelayedTask(this, () ->
                getServer().getOnlinePlayers().forEach(player -> {
                    MwPlayer mwPlayer = mwPlayerRegistry.getOrCreate(player);
                    this.callEvent(new MwPlayerChangeClassEvent(mwPlayer));
                })
        , 5L);

        new SkinListener(this).register();

        configurateWorld();
    }

    public MwPlayerRegistry getMwPlayerRegistry(){
        return mwPlayerRegistry;
    }

    public WorldConfiguration getWorldConfiguration(){
        return worldConfig;
    }

    public void callEvent(Event event){
        getServer().getPluginManager().callEvent(event);
    }

    private void configurateWorld(){
        worldConfig.setBlockBreaking(true);
        worldConfig.setBlockBurning(false);
        worldConfig.setBlockPlacing(true);
        worldConfig.setDaylightCycle(false);
        worldConfig.setExperienceChange(false);
        worldConfig.setFoodChange(false);
        worldConfig.setFriendlyBowFire(false);
        worldConfig.setKeepInventory(false);
        worldConfig.setMobSpawning(false);
        worldConfig.setAdditionalNaturalRegen(false);
        worldConfig.setWeatherChange(false);

        for(World world : getServer().getWorlds()){
            world.setTime(20000);
            world.setWeatherDuration(0);
            world.setThundering(false);
            world.setStorm(false);
            world.setDifficulty(Difficulty.EASY);
        }
    }

}
