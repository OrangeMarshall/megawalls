package com.orangemarshall.mw.util;

import com.orangemarshall.mw.MegaWalls;
import org.bukkit.Bukkit;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;


public class WorldConfigListener extends SimpleListener implements WorldConfiguration{

    private boolean allowFriendlyProjectileFire = false;
    private boolean allowWeatherChange = false;
    private boolean allowExperienceChange = false;
    private boolean allowBlockPlacing = false;
    private boolean allowBlockBreaking = false;
    private boolean allowBlockBurning = false;
    private boolean allowAdditionalNaturalRegen = false;
    private boolean allowFoodChange = false;

    public WorldConfigListener(MegaWalls plugin){
        super(plugin);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onBow(EntityDamageByEntityEvent event){
        if(!allowFriendlyProjectileFire){
            if(event.getDamager() instanceof Projectile){
                Projectile projectile = (Projectile) event.getDamager();

                if(projectile.getShooter() == event.getEntity()){
                    event.setCancelled(true);
                }
            }
        }
    }

    @Override
    public void setFriendlyBowFire(boolean allowFriendlyProjectileFire){
        this.allowFriendlyProjectileFire = allowFriendlyProjectileFire;
    }


    @Override
    public void setKeepInventory(boolean keepInventory){
        dispatchGamerule("doDaylightCycle " + keepInventory);
    }


    @EventHandler
    public void onExp(PlayerExpChangeEvent event){
        if(!allowExperienceChange){
            event.setAmount(0);
        }
    }

    @Override
    public void setExperienceChange(boolean allowExperienceChange){
        this.allowExperienceChange = allowExperienceChange;
    }


    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event){
        if(!allowBlockPlacing){
            event.setCancelled(true);
        }
    }

    @Override
    public void setBlockPlacing(boolean allowBlockPlacing){
        this.allowBlockPlacing = allowBlockPlacing;
    }


    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){
        if(!allowBlockBreaking){
            event.setCancelled(true);
        }
    }

    @Override
    public void setBlockBreaking(boolean allowBlockBreaking){
        this.allowBlockBreaking = allowBlockBreaking;
    }


    @EventHandler
    public void onBlockBurn(BlockBurnEvent event){
        if(!allowBlockBurning){
            event.setCancelled(true);
        }
    }

    @Override
    public void setBlockBurning(boolean allowBlockBurning){
        this.allowBlockBurning = allowBlockBurning;
    }


    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent event){
        if(!allowFoodChange){
            event.setCancelled(true);
        }
    }

    @Override
    public void setFoodChange(boolean allowFoodChange){
        this.allowFoodChange = allowFoodChange;
    }


    @EventHandler
    public void onRegen(EntityRegainHealthEvent event){
        if(!allowAdditionalNaturalRegen){
            if(event.getRegainReason() == EntityRegainHealthEvent.RegainReason.REGEN){
                event.setAmount(0);
            }
        }
    }

    @Override
    public void setAdditionalNaturalRegen(boolean allowAdditionalNaturalRegen){
        this.allowAdditionalNaturalRegen = allowAdditionalNaturalRegen;
    }


    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event){
        if(!allowWeatherChange){
            event.setCancelled(true);
        }
    }

    @Override
    public void setWeatherChange(boolean allowWeatherChange){
        this.allowWeatherChange = allowWeatherChange;
    }


    @Override
    public void setDaylightCycle(boolean allowDaylightCycle){
        dispatchGamerule("doDaylightCycle " + allowDaylightCycle);
    }

    @Override
    public void setMobSpawning(boolean allowMobSpawning){
        dispatchGamerule("doMobSpawning " + allowMobSpawning);
    }

    private void dispatchGamerule(String args){
        Bukkit.getScheduler().runTask(plugin, ()-> {
            plugin.getServer().dispatchCommand(Bukkit.getConsoleSender(), "gamerule " + args);
        });
    }

}
