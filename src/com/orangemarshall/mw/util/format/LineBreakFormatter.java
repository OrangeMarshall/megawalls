package com.orangemarshall.mw.util.format;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.List;


public class LineBreakFormatter{

    private int maxChars;

    public LineBreakFormatter(int maxChars){
        this.maxChars = maxChars;
    }

    public String[] formatToArray(String line){
        List<String> lines = formatToList(line);
        return lines.toArray(new String[lines.size()]);
    }

    public List<String> formatToList(String line){
        List<String> lines = Lists.newArrayList();
        String remainingContent = line.trim();

        while(!remainingContent.isEmpty()){
            int nextCharCount = getNextLineLength(remainingContent);
            String nextLine = remainingContent.substring(0, nextCharCount).trim();
            remainingContent = remainingContent.substring(nextCharCount).trim();

            lines.add(nextLine);
        }

        return lines;
    }

    public String formatToString(String line, String splitter){
        return StringUtils.join(formatToList(line), splitter);
    }

    private int getNextLineLength(String remainingContent){
        if(remainingContent.length() <= maxChars){
            return remainingContent.length();
        }

        if(remainingContent.contains(" ")){
            return remainingContent.substring(0, maxChars).lastIndexOf(" ");
        }else{
            return maxChars;
        }
    }

}
