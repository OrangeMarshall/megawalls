package com.orangemarshall.mw.util;

import com.orangemarshall.mw.MegaWalls;


public abstract class PluginHolder{

    protected final MegaWalls plugin;

    public PluginHolder(MegaWalls plugin){
        this.plugin = plugin;
    }

}
