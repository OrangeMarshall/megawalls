package com.orangemarshall.mw.util;

import com.orangemarshall.util.ItemStacks;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.function.Predicate;


public class MwUtil{

    public static void makeKitItem(ItemStack itemStack){
        String displayName = CraftItemStack.asNMSCopy(itemStack).getName();
        ItemMeta itemMeta = itemStack.getItemMeta();

        //TODO replace with custom item class
        itemMeta.setDisplayName(ChatColor.AQUA + "Kit " + displayName);

        itemStack.setItemMeta(itemMeta);
    }

    public static ItemStack makeKitItem(Material material){
        ItemStack itemStack = new ItemStack(material);
        makeKitItem(itemStack);
        return itemStack;
    }

    public static boolean isKitItem(ItemStack itemStack){
        if(itemStack == null){
            return false;
        }

        ItemMeta meta = itemStack.getItemMeta();

        return meta != null && meta.hasDisplayName() && meta.getDisplayName().startsWith(ChatColor.AQUA + "Kit ");
    }

    public static void removeItems(Player player, Predicate<ItemStack> removeIfTrue){
            ItemStack[] newContents = player.getInventory().getContents();
            removeItems(newContents, removeIfTrue);
            player.getInventory().setContents(newContents);

            ItemStack[] newArmorContents = player.getInventory().getArmorContents();
            removeItems(player.getInventory().getArmorContents(), removeIfTrue);
            player.getInventory().setArmorContents(newArmorContents);
    }

    private static void removeItems(ItemStack[] itemStack, Predicate<ItemStack> removeIfTrue){
        for(int i = 0; i < itemStack.length; i++){
            if(removeIfTrue.test(itemStack[i])){
                itemStack[i] = null;
            }
        }
    }

    public static void equipItems(Player player, List<ItemStack> items){
        PlayerInventory inventory = player.getInventory();

        for(ItemStack item : items){
            makeKitItem(item);

            int armorIndex = ItemStacks.getArmorIndex(item.getType());

            if(armorIndex > 0){
                ItemStack[] armor = inventory.getArmorContents();
                armor[armorIndex] = item;
                inventory.setArmorContents(armor);
            }else{
                inventory.addItem(item);
            }
        }
    }

}
