package com.orangemarshall.mw.util;

import com.orangemarshall.mw.MegaWalls;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;


public abstract class SimpleListener extends PluginHolder implements Listener{

    public SimpleListener(MegaWalls plugin){
        super(plugin);
    }

    public void register(){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public void unregister(){
        HandlerList.unregisterAll(this);
    }

}
