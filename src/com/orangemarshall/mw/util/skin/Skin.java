package com.orangemarshall.mw.util.skin;

import org.bukkit.Bukkit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.Scanner;
import java.util.logging.Level;

public class Skin {

    private String uuid;
    private String name;
    private String value;
    private String signatur;
    private String username;
    private String url;

    private Skin(String uuid){
        this.uuid = uuid;
    }

    public String getUuid(){
        return uuid;
    }

    public static Skin fromData(String uuid){
        Skin skin = new Skin(uuid);
        skin.load();
        return skin;
    }

    public String getSkinOwner(){
        return username;
    }

    public String getSkinOwnerUuid(){
        return uuid;
    }

    private void load(){
        try {
            String urlString = "https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false";
            URL url = new URL(urlString);
            URLConnection uc = url.openConnection();
            uc.setUseCaches(false);
            uc.setDefaultUseCaches(false);
            uc.addRequestProperty("User-Agent", "Mozilla/5.0");
            uc.addRequestProperty("Cache-Control", "no-cache, no-store, must-revalidate");
            uc.addRequestProperty("Pragma", "no-cache");

            // Parse it

            Thread.sleep(2000);
            String json = new Scanner(uc.getInputStream(), "UTF-8").useDelimiter("\\A").next();
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(json);
            JSONArray properties = (JSONArray) obj.get("properties");

            this.username = (String) obj.get("name");

            for (int i = 0; i < properties.size(); i++) {
                try {
                    JSONObject property = (JSONObject) properties.get(i);
                    String name = (String) property.get("name");
                    String value = (String) property.get("value");
                    String signature = property.containsKey("signature") ? (String) property.get("signature") : null;

                    this.name = name;
                    this.value = value;
                    this.signatur = signature;

                    String decoded = new String(Base64.getDecoder().decode(value));
                    JSONObject valueObj = (JSONObject) new JSONParser().parse(decoded);
                    this.url = (String) ((JSONObject) ((JSONObject)valueObj.get("textures")).get("SKIN")).get("url");
                } catch (Exception e) {
                    Bukkit.getLogger().log(Level.WARNING, "Failed to apply auth property", e);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Failed to load skin
        }
    }

    public String getSkinUrl(){
        return url;
    }

    public String getSkinValue(){
        return value;
    }

    public String getSkinName(){
        return name;
    }

    public String getSkinSignatur(){
        return signatur;
    }

}
