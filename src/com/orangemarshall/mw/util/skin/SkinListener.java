package com.orangemarshall.mw.util.skin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;
import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.SkinEnum;
import com.orangemarshall.mw.util.SimpleListener;
import com.orangemarshall.util.FieldWrapper;
import com.orangemarshall.util.Util;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;

import java.util.List;


public class SkinListener extends SimpleListener{

    private static final FieldWrapper<WrappedGameProfile> gameProfileWrapper = new FieldWrapper<>("profile", PlayerInfoData.class);

    public SkinListener(MegaWalls plugin){
        super(plugin);
    }

    public void register(){
        super.register();
        ProtocolLibrary.getProtocolManager().addPacketListener(new SkinPacketAdapter(plugin, PacketType.Play.Server.PLAYER_INFO));
    }

    private void setClassSkin(Player player){
        MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate(player);
        SkinEnum skin = mwPlayer.getMwClassInstance().getClassEnum().getSkin();

        setSkin(mwPlayer.player, skin);
    }

    private void setSkin(Player player, SkinEnum skin){
        PropertyMap propertyMap = ((CraftPlayer) player).getProfile().getProperties();
        propertyMap.removeAll("textures");
        propertyMap.put("textures", new Property("textures", skin.getValue(), skin.getSignature()));
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event){
        setClassSkin(event.getPlayer());
        PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, ((CraftPlayer)event.getPlayer()).getHandle());
        Util.sendPacket(event.getPlayer(), packet);
    }


    private class SkinPacketAdapter extends PacketAdapter{

        public SkinPacketAdapter(Plugin plugin, PacketType... types) {
            super(plugin, types);
        }

        @Override
        public void onPacketSending(PacketEvent event){
            for(List<PlayerInfoData> playerInfoDatas : event.getPacket().getPlayerInfoDataLists().getValues()){
                for(PlayerInfoData playerInfoData : playerInfoDatas){
                    CraftPlayer player = (CraftPlayer) Bukkit.getPlayer(playerInfoData.getProfile().getName());
                    if(player != null){
                        setClassSkin(player);
                        gameProfileWrapper.set(playerInfoData, WrappedGameProfile.fromPlayer(player));
                    }
                }
            }
        }

    }

}

