package com.orangemarshall.mw.util;

public interface WorldConfiguration{

    void setFriendlyBowFire(boolean allowFriendlyBowFire);
    void setKeepInventory(boolean keepInventory);
    void setExperienceChange(boolean allowExperienceChange);
    void setBlockPlacing(boolean allowBlockPlacing);
    void setBlockBreaking(boolean allowBlockBreaking);
    void setBlockBurning(boolean allowBlockBurning);
    void setFoodChange(boolean allowFoodChange);
    void setAdditionalNaturalRegen(boolean allowAdditionalNaturalRegen);
    void setWeatherChange(boolean allowWeatherChange);
    void setDaylightCycle(boolean allowDaylightCycle);
    void setMobSpawning(boolean allowMobSpawning);

    //TODO add global potioneffects

}
