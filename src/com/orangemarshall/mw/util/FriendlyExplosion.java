package com.orangemarshall.mw.util;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.orangemarshall.util.FieldWrapper;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.event.CraftEventFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


public class FriendlyExplosion extends Explosion{

    private final World world;

    private final double posX;
    private final double posY;
    private final double posZ;

    private final float size;
    /*
    private final List<BlockPosition> blocks = Lists.newArrayList();
    */private final Map<EntityHuman, Vec3D> k = Maps.newHashMap();

    private Collection<Entity> friendly;
    private FieldWrapper<List<BlockPosition>> blockWrapper = new FieldWrapper<>("blocks", Explosion.class);

    public FriendlyExplosion(Location location, float damage, boolean setOnFire, boolean destroyBlocks, Collection<Entity> friendly){
        this(((CraftWorld)location.getWorld()).getHandle(), null, location.getX(), location.getY(), location.getZ(), damage, setOnFire, destroyBlocks);
        this.friendly = friendly;
    }

    private FriendlyExplosion(World world, Entity entity, double d0, double d1, double d2, float f, boolean flag, boolean flag1) {
        super(world, entity, d0, d1, d2, f, flag, flag1);
        this.world = world;
        this.size = (float)Math.max((double)f, 0.0D);
        this.posX = d0;
        this.posY = d1;
        this.posZ = d2;
    }

    public void a() {
        if(this.size >= 0.1F) {
            HashSet<BlockPosition> hashset = Sets.newHashSet();

            int i;
            int j;
            for(int f3 = 0; f3 < 16; ++f3) {
                for(i = 0; i < 16; ++i) {
                    for(j = 0; j < 16; ++j) {
                        if(f3 == 0 || f3 == 15 || i == 0 || i == 15 || j == 0 || j == 15) {
                            double d0 = (double)((float)f3 / 15.0F * 2.0F - 1.0F);
                            double d1 = (double)((float)i / 15.0F * 2.0F - 1.0F);
                            double d2 = (double)((float)j / 15.0F * 2.0F - 1.0F);
                            double d3 = Math.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
                            d0 /= d3;
                            d1 /= d3;
                            d2 /= d3;
                            float f = this.size * (0.7F + this.world.random.nextFloat() * 0.6F);
                            double d4 = this.posX;
                            double d5 = this.posY;

                            for(double d6 = this.posZ; f > 0.0F; f -= 0.22500001F) {
                                BlockPosition blockposition = new BlockPosition(d4, d5, d6);
                                IBlockData iblockdata = this.world.getType(blockposition);
                                if(iblockdata.getBlock().getMaterial() != Material.AIR) {
                                    float f2 = this.source != null?this.source.a(this, this.world, blockposition, iblockdata):iblockdata.getBlock().a((Entity)null);
                                    f -= (f2 + 0.3F) * 0.3F;
                                }

                                if(f > 0.0F && (this.source == null || this.source.a(this, this.world, blockposition, iblockdata, f)) && blockposition.getY() < 256 && blockposition.getY() >= 0) {
                                    hashset.add(blockposition);
                                }

                                d4 += d0 * 0.30000001192092896D;
                                d5 += d1 * 0.30000001192092896D;
                                d6 += d2 * 0.30000001192092896D;
                            }
                        }
                    }
                }
            }

            blockWrapper.get(this).addAll(hashset);

            float var48 = this.size * 2.0F;
            i = MathHelper.floor(this.posX - (double)var48 - 1.0D);
            j = MathHelper.floor(this.posX + (double)var48 + 1.0D);
            int l = MathHelper.floor(this.posY - (double)var48 - 1.0D);
            int i1 = MathHelper.floor(this.posY + (double)var48 + 1.0D);
            int j1 = MathHelper.floor(this.posZ - (double)var48 - 1.0D);
            int k1 = MathHelper.floor(this.posZ + (double)var48 + 1.0D);
            List<Entity> list = this.world.getEntities(this.source, new AxisAlignedBB((double)i, (double)l, (double)j1, (double)j, (double)i1, (double)k1));
            Vec3D vec3d = new Vec3D(this.posX, this.posY, this.posZ);

            list.removeAll(friendly);

            for(int l1 = 0; l1 < list.size(); ++l1) {
                Entity entity = list.get(l1);
                if(!entity.aW()) {
                    double d7 = entity.f(this.posX, this.posY, this.posZ) / (double)var48;
                    if(d7 <= 1.0D) {
                        double d8 = entity.locX - this.posX;
                        double d9 = entity.locY + (double)entity.getHeadHeight() - this.posY;
                        double d10 = entity.locZ - this.posZ;
                        double d11 = (double)MathHelper.sqrt(d8 * d8 + d9 * d9 + d10 * d10);
                        if(d11 != 0.0D) {
                            d8 /= d11;
                            d9 /= d11;
                            d10 /= d11;
                            double d12 = (double)this.world.a(vec3d, entity.getBoundingBox());
                            double d13 = (1.0D - d7) * d12;
                            CraftEventFactory.entityDamage = this.source;
                            entity.forceExplosionKnockback = false;
                            boolean wasDamaged = entity.damageEntity(DamageSource.explosion(this), (float)((int)((d13 * d13 + d13) / 2.0D * 8.0D * (double)var48 + 1.0D)));
                            CraftEventFactory.entityDamage = null;
                            if(wasDamaged || entity instanceof EntityTNTPrimed || entity instanceof EntityFallingBlock || entity.forceExplosionKnockback) {
                                double d14 = EnchantmentProtection.a(entity, d13);
                                entity.motX += d8 * d14;
                                entity.motY += d9 * d14;
                                entity.motZ += d10 * d14;
                                if(entity instanceof EntityHuman && !((EntityHuman)entity).abilities.isInvulnerable) {
                                    this.k.put((EntityHuman)entity, new Vec3D(d8 * d13, d9 * d13, d10 * d13));
                                }
                            }
                        }
                    }
                }
            }

        }
    }

}
