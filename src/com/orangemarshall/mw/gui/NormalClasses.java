package com.orangemarshall.mw.gui;

import com.orangemarshall.mw.clazz.ClassType;
import com.orangemarshall.util.gui.GuiController;


public class NormalClasses extends ClassSelectorScreen{

    public NormalClasses(GuiController guiController){
        super(guiController, ClassType.NORMAL);
    }

}
