package com.orangemarshall.mw.gui;

import com.orangemarshall.util.gui.screen.LayeredGuiScreen;
import com.orangemarshall.util.gui.GuiController;


public class GlobalCosmetics extends LayeredGuiScreen{

    public GlobalCosmetics(GuiController guiController){
        super(guiController);
    }

    @Override
    public String getTitle(){
        return "Global Cosmetics";
    }

}
