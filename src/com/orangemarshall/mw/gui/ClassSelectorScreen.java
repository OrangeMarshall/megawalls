package com.orangemarshall.mw.gui;

import com.google.common.collect.Lists;
import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.ClassType;
import com.orangemarshall.mw.clazz.MwClassEnum;
import com.orangemarshall.mw.MwFactory;
import com.orangemarshall.mw.clazz.skill.PlayerAbility;
import com.orangemarshall.util.ItemStacks;
import com.orangemarshall.util.Notifier;
import com.orangemarshall.util.Util;
import com.orangemarshall.mw.util.format.LoreFormatter;
import com.orangemarshall.util.gui.GuiController;
import com.orangemarshall.util.gui.element.GuiElement;
import com.orangemarshall.util.gui.screen.PagedListGuiScreen;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.stream.Collectors;


public class ClassSelectorScreen extends PagedListGuiScreen{

    private String name;

    public ClassSelectorScreen(GuiController guiController, ClassType classType){
        super(guiController, classType.getClassesOfType().stream().map(mwClass -> new ClassElement(mwClass, guiController)).collect(Collectors.toList()));
        this.name = classType.getNiceName();
    }

    @Override
    public String getTitle(){
        return name + " Classes";
    }

    private static class ClassElement implements GuiElement{

        private MwClassEnum mwClass;
        private GuiController guiController;
        private ItemStack skull;

        public ClassElement(MwClassEnum mwClass, GuiController guiController){
            this.mwClass = mwClass;
            this.guiController = guiController;
            this.skull = mwClass.getSkull();
        }

        @Override
        public void onClick(){
            MwPlayer mwPlayer = ((MegaWalls) guiController.getPlugin()).getMwPlayerRegistry().getOrCreate(guiController.getPlayer());

            if(isSelected()){
                new Notifier().toPlayer(mwPlayer.player)
                        .withSound(Sound.CLICK)
                        .send(ChatColor.RED + "You already have the " + ChatColor.YELLOW + mwClass.getNiceName() + ChatColor.RED + " class selected.");
                guiController.closeGuiAndInventory();
            }else{
                mwPlayer.changeMwClass(mwClass);
                mwPlayer.player.playSound(mwPlayer.player.getLocation(), Sound.CLICK, 1.0f, 1.0f);
                guiController.updateInventory();
            }

        }

        @Override
        public ItemStack getItemStack(){    //TODO easier visualization
            String clDesc = mwClass.getInformation().getDescription();

            PlayerAbility ability = MwFactory.createDummySkillSetInstance(mwClass).getAbility();
            String abDesc = ability.getDescription();
            String abName = ability.getName();

            List<String> classDescription = new LoreFormatter().formatToList(clDesc);
            List<String> abilityDescription = new LoreFormatter().formatToList(abDesc);
            abilityDescription.add(0, "Ability: " + ChatColor.WHITE.toString() + ChatColor.BOLD + abName);

            List<String> lore = Lists.newArrayList();
            lore.addAll(classDescription);
            lore.add("");
            lore.addAll(abilityDescription);
            lore.add("");

            lore = Util.addPrefixToEach(lore, ChatColor.GRAY.toString());

            if(isSelected()){
                lore.add(ChatColor.GREEN + "SELECTED!");
            }else{
                lore.add(ChatColor.YELLOW + "Click to select!");
            }

            ItemStacks.setName(skull, ChatColor.WHITE.toString() + ChatColor.BOLD + mwClass.getNiceName(), lore.toArray(new String[lore.size()]));

            return skull;
        }

        private boolean isSelected(){
            MwPlayer mwPlayer = ((MegaWalls) guiController.getPlugin()).getMwPlayerRegistry().getOrCreate(guiController.getPlayer());
            return mwPlayer.getMwClassInstance().getClassEnum() == mwClass;
        }

    }

}
