package com.orangemarshall.mw.gui;

import com.orangemarshall.mw.clazz.ClassType;
import com.orangemarshall.util.gui.GuiController;


public class MythicClasses extends ClassSelectorScreen{

    public MythicClasses(GuiController guiController){
        super(guiController, ClassType.MYTHIC);
    }

}
