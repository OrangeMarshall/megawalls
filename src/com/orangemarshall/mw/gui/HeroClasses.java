package com.orangemarshall.mw.gui;

import com.orangemarshall.mw.clazz.ClassType;
import com.orangemarshall.util.gui.GuiController;


public class HeroClasses extends ClassSelectorScreen{

    public HeroClasses(GuiController guiController){
        super(guiController, ClassType.HERO);
    }

}
