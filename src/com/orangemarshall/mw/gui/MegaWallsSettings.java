package com.orangemarshall.mw.gui;

import com.orangemarshall.util.gui.screen.LayeredGuiScreen;
import com.orangemarshall.util.gui.GuiController;


public class MegaWallsSettings extends LayeredGuiScreen{

    public MegaWallsSettings(GuiController guiController){
        super(guiController);
    }

    @Override
    public String getTitle(){
        return "Mega Walls Settings";
    }

}
