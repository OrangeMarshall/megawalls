package com.orangemarshall.mw.gui;

import com.orangemarshall.util.gui.screen.LayeredGuiScreen;
import com.orangemarshall.util.gui.GuiController;


public class Miscellaneous extends LayeredGuiScreen{

    public Miscellaneous(GuiController guiController){
        super(guiController);
    }

    @Override
    public String getTitle(){
        return "Miscellaneous";
    }
}
