package com.orangemarshall.mw.gui;

import com.orangemarshall.mw.MegaWalls;
import com.orangemarshall.mw.MwPlayer;
import com.orangemarshall.mw.clazz.ClassType;
import com.orangemarshall.mw.clazz.MwClassEnum;
import com.orangemarshall.util.ItemStacks;
import com.orangemarshall.util.gui.GuiController;
import com.orangemarshall.util.gui.element.OpenScreen;
import com.orangemarshall.util.gui.screen.LayeredGuiScreen;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.Optional;


public class MainMenu extends LayeredGuiScreen{

    public MainMenu(GuiController guiController){
        super(guiController);
    }

    @Override
    public void drawScreen(Inventory inventory){
        loadElements();
        
        super.drawScreen(inventory);
    }

    private String[] getClassList(ClassType classType, MwClassEnum selectedClass){
        return getClassList(classType, Optional.of(selectedClass));
    }

    private String[] getClassList(ClassType classType, Optional<MwClassEnum> selectedClass){
        StringBuilder builder = new StringBuilder();
        MwClassEnum highlightedClass = selectedClass.isPresent() ? selectedClass.get() : null;
        Collection<MwClassEnum> classEna = classType.getClassesOfType();

        classEna.forEach(classEnum -> {
            String name = WordUtils.capitalizeFully(classEnum.name());

            if(classEnum == highlightedClass){
                name = ChatColor.GRAY + name;
            }else{
                name = ChatColor.DARK_GRAY + name;
            }

            name += "\n";

            builder.append(name);
        });

        builder.append(" \n" + ChatColor.YELLOW + "Click to browse!");

        return builder.toString().split("\n");
    }

    private void loadElements(){
        MegaWalls plugin = (MegaWalls) guiController.getPlugin();
        Player player = guiController.getPlayer();

        int maxRowIndex = guiController.getRows()-1;
        int maxColumnIndex = guiController.getColumns()-1;

        addElement(new Coins(), maxRowIndex, maxColumnIndex/2 + (maxColumnIndex&1));

        MwPlayer mwPlayer = plugin.getMwPlayerRegistry().getOrCreate(player);
        MwClassEnum selectedClass = mwPlayer.getMwClassInstance().getClassEnum();

        ItemStack normalClasses = ItemStacks.Builder
                .create(Material.IRON_SWORD)
                .setName(ChatColor.GREEN + "Normal Classes " + ChatColor.DARK_GRAY)
                .setLore(getClassList(ClassType.NORMAL, selectedClass))
                .build();
        addElement(new OpenScreen(guiController, new NormalClasses(guiController), normalClasses), 1, 1);

        ItemStack heroClasses = ItemStacks.Builder
                .create(Material.DIAMOND_SWORD)
                .setName(ChatColor.GREEN + "Hero Classes ")
                .setLore(getClassList(ClassType.HERO, selectedClass))
                .build();
        addElement(new OpenScreen(guiController, new HeroClasses(guiController), heroClasses), 1, 4);

        ItemStack mythicClasses = ItemStacks.Builder
                .create(Material.GOLD_SWORD)
                .setName(ChatColor.GREEN + "Mythic Classes ")
                .setLore(getClassList(ClassType.MYTHIC, selectedClass))
                .build();
        addElement(new OpenScreen(guiController, new MythicClasses(guiController), mythicClasses), 1, 7);

        ItemStack globalCosmetics = ItemStacks.Builder
                .create(Material.CHEST)
                .setName(ChatColor.GREEN + "Global Cosmetics")
                .setLore(new String[]{
                        ChatColor.GRAY + "Unlock and activate cosmetics. These choices are",
                        ChatColor.GRAY + "not affected by your class selection.",
                        "",
                        ChatColor.YELLOW + "Click to browse!"
                })
                .build();
        addElement(new OpenScreen(guiController, new GlobalCosmetics(guiController), globalCosmetics), 3, 2);

        ItemStack mwSettings = ItemStacks.Builder
                .create(Material.REDSTONE_COMPARATOR)
                .setName(ChatColor.GREEN + "Mega Walls Settings")
                .setLore(new String[]{
                        ChatColor.GRAY + "Allows you to edit and control various settings",
                        ChatColor.GRAY + "related to your overall Mega Walls experience.",
                        ChatColor.GRAY + "Contains color blind mode, particle toggles,",
                        ChatColor.GRAY + "chat toggles, and more!",
                        "",
                        ChatColor.YELLOW + "Click to browse!"
                })
                .build();
        addElement(new OpenScreen(guiController, new MegaWallsSettings(guiController), mwSettings), 3, 4);

        ItemStack misc = ItemStacks.Builder
                .create(Material.STORAGE_MINECART)
                .setName(ChatColor.GREEN + "Miscellaneous")
                .setLore(new String[]{
                        ChatColor.GRAY + "Perks and features that you",
                        ChatColor.GRAY + "can unlock or upgrade.",
                        "",
                        ChatColor.YELLOW + "Click to browse!"
                })
                .build();
        addElement(new OpenScreen(guiController, new Miscellaneous(guiController), misc), 3, 6);
    }


    @Override
    public String getTitle(){
        return "Mega Walls";
    }
}
