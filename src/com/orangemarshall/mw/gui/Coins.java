package com.orangemarshall.mw.gui;

import com.orangemarshall.util.ItemStacks;
import com.orangemarshall.util.gui.element.GuiElement;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class Coins implements GuiElement{

    @Override
    public void onClick(){}

    @Override
    public ItemStack getItemStack(){
        ItemStack item = new ItemStack(Material.EMERALD);

        String name = ChatColor.GRAY + "Total Coins: " + ChatColor.GOLD + "1118865";
        String[] lore = {
                ChatColor.GOLD + "http://store.hypixel.net"
        };

        ItemStacks.setName(item, name, lore);
        return item;
    }
}
