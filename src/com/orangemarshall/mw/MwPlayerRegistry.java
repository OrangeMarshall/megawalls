package com.orangemarshall.mw;

import com.google.common.collect.Maps;
import com.orangemarshall.mw.util.PluginHolder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Map;


public class MwPlayerRegistry extends PluginHolder{

    private final Map<Player, MwPlayer> players = Maps.newHashMap();

    public MwPlayerRegistry(MegaWalls plugin){
        super(plugin);
        Bukkit.getOnlinePlayers().forEach(this::getOrCreate);
    }

    public Collection<MwPlayer> getOnlinePlayers(){
        return players.values();
    }

    public MwPlayer removePlayer(Player player){
        return players.remove(player);
    }

    public MwPlayer getOrCreate(Player player) {
        MwPlayer mwPlayer = players.get(player);

        if(mwPlayer == null){
            mwPlayer = new MwPlayer(this, player);
            players.put(player, mwPlayer);
        }

        return mwPlayer;
    }

    public MegaWalls getPlugin(){
        return plugin;
    }

}
